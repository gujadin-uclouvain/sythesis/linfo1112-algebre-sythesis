%----------------------------------------------------------------------------------------
%	PART 4
%----------------------------------------------------------------------------------------
\lhead{ESPACES EUCLIDIENS}
\part{Espaces euclidiens}
Dans certains espaces vectoriels, tels que l’espace usuel, il est possible de calculer la longueur d’un vecteur ou l’angle entre deux vecteurs au moyen d’un produit scalaire.\\
Dans cette section, nous allons nous concentrer sur le \textbf{produit scalaire}, et non sur la norme.

\section{Produits scalaires}
\subsection*{Définitions}
\label{sec:def_prod_scal}
\paragraph{Une Forme Bilinéaire Symétrique}
sur un espace vectoriel $E$, sur le corps $\mathbb{R}$ est une application
\begin{center}
$(\cdot|\cdot) : E \times E \rightarrow \mathbb{R}$
\end{center}
satisfaisant les conditions suivantes :

1. \underline{Bilinéarité} : pour $x,y,z \in E$ et $\alpha,\beta \in \mathbb{R}$,
\begin{center}
$\begin{array}{cl}
(x\alpha + y\beta |z) &=\ \alpha (x|z) + \beta (y|z).\\
(z|x\alpha + y\beta ) &=\ (z|x)\alpha + (z|y)\beta.
\end{array}$
\end{center}

2. \underline{Symétrie} : pour $x,y \in E$,
\begin{center}
$(y|x) = (x|y)$.
\end{center}
\paragraph{Un Produit Scalaire}
est une forme bilinéaire symétrique qui possède EN PLUS la propriété suivante:

3. \underline{Définie positivité} : pour $x \in E, x \neq 0$,
\begin{center}
$(x|x) > 0$.
\end{center}
La bilinéarité entraîne par ailleurs
\begin{center}
$(0|0) = 0$.
\end{center}
\paragraph{Un Espace Euclidien}
est un espace muni d'un produit scalaire.
\paragraph{Autres notations du produit scalaire}
\begin{center}
$(x|y)\quad \Leftrightarrow\quad x\cdot y\quad \Leftrightarrow\quad (x,y)\quad \Leftrightarrow\quad \langle x,y\rangle\quad \Leftrightarrow\quad \langle x|y\rangle$
\end{center}
\subsection*{Exemples}
Le produit scalaire usuel sur $\mathbb{R}^n$ est noté et défini par
\begin{center}
$((x_1,\cdots ,x_n)\ |\ (y_1,\cdots ,y_n)) = x_1y_1 + \cdots + x_ny_n$.
\end{center}
D’autres formes bilinéaires symétriques peuvent être définies de la manière suivante:\\
Soit $A \in \mathbb{R}^n$ une \hyperref[sec:type-matrice]{matrice symétrique}, c'est-à-dire telle que $A^t = A$ et $x,y \in \mathbb{R}^{n\times 1}$,
\begin{center}
$(x|y)_A = x^t \cdot A \cdot y$.
\end{center}
La bilinéarité de $(\cdot |\cdot )_A$ résulte des propriétés du produit matriciel ainsi que de l'hypothèse que $A$ est symétrique.\\
Cela permet d'assurer que
\begin{center}
$(y|x)_A = (x|y)_A\quad \quad$ pour $x,y \in \mathbb{R}^n$.
\end{center}
De plus, l'on peut aussi définir une \hyperref[sec:def_prod_scal]{forme bilinéaire symétrique} sur des espaces , des ensembles (des fonctions continues sur une intervalle [intégrales]) ou sur d'autres scalaires.

\paragraph*{\textbf{Comparaison de concepts:}} Le produit scalaire peut être comparé, en programmation, à une fonction de deux arguments noté $(a|b)$ où $a$ et $b$ peuvent être des fonctions, des réels, des inconnues,...
\subparagraph*{Exemple:} Si
\begin{center}
$\begin{array}{ccc}
a &=& 1-x^2\\
b &=& x\\
(a|b) &=& \int_0^1 ab.
\end{array}$
\end{center}

Alors,
\begin{center}
$\begin{array}{ccc}
(a|b) &=& \int_0^1 (1-x^2)\cdot x\\
(a|b) &= &\dfrac{1}{4}
\end{array}$
\end{center}

\subsection{Normes et distances}
Dans un espace euclidien $E$, on définit la \textbf{norme} d'un vecteur $x$ par
\begin{center}
$\Vert x \Vert = \sqrt{(x|x)} \in \mathbb{R}$\\
$\Vert x \Vert^2 = (x|x) \in \mathbb{R}$
\end{center}
La norme, c'est la "longueur" du vecteur.
\paragraph{Proposition}
L'application $\Vert \cdot \Vert : E \rightarrow \mathbb{R}$ satisfait les conditions qui définissent une norme (abstraite) au sens de l'analyse, à savoir:
\begin{center}
$\begin{array}{rl}
(I) &\Vert x\Vert \geq 0$ pour tout $x \in E.\\
(II) &\Vert x\Vert = 0$ entraîne $x = 0$ \underline{(où 0 est un vecteur)}$.\\
(III) &\Vert \alpha x\Vert = |\alpha | \cdot \Vert x\Vert$ pour $x \in E$ et $\alpha \in \mathbb{R}.\\
(IV) &\Vert x + y\Vert \leq \Vert x\Vert + \Vert y\Vert$ pour $x,y \in E.
\end{array}$
\end{center}
\paragraph{Démonstration}
:\\
(I) est claire,\\
(II) découle directement de la \hyperref[sec:def_prod_scal]{définie positivité du produit scalaire},\\
(III) résulte de sa linéarité,\\
(IV) doit utiliser le lemme suivant pour être établie:
\paragraph{Lemme (Inégalité de Cauchy-Schwarz).}
Pour $x,y \in E$,
\begin{center}
$(x|y) \leq |(x|y)| \leq \Vert x\Vert\cdot \Vert y\Vert$.
\end{center}

\section{Bases}
\subsection{Bases orthogonales}
\paragraph{Définition\\}
\textbf{Deux vecteurs} $x,y$ d'un espace euclidien $E$ sont dits \textbf{orthogonaux} si
\begin{center}
$(x|y) = 0$.
\end{center}
\textbf{NB:} Comme $(y|x) = (x|y)$, cette relation est \underline{symétrique}.\\

\paragraph{Application\\}
Une \textbf{base} $e = (e_1,\cdots ,e_n)$ d'un espace euclidien $E$ est dite \textbf{orthogonale} si les vecteurs de base sont orthogonaux deux à deux (toute base réduite à un élément est considérée comme orthogonale) :
\begin{center}
$(e_i|e_j) = 0 \quad$ si $i \neq j$.
\end{center}

\subsection{Bases orthonormées}
\paragraph{Théorème\\}
Tout espace euclidien de dimension finie non nulle admet une base orthonormée.

\paragraph{Exemple\\}
Il suffit de de prouver l'existence d'une base orthonormée:
\begin{itemize}
\item Si l'espace est de dimension 1, le théorème est clair.
\item On peut donc supposer que $\dim E \geqslant 2$, raisonner par induction et supposer que l'énoncé est vrai pour les espaces euclidiens de dimension < $\dim E$.
\end{itemize}

\paragraph{Application\\}
Une \underline{\textbf{base orthogonale}} $e = (e_1,\cdots ,e_n)$ d'un espace euclidien $E$ est dite \textbf{orthonormée} si la norme de tout les vecteurs de base sont égaux à 1:
\begin{center}
$||e_i|| = 1 \quad$ $\forall\ i = 1,\cdots ,n$.
\end{center}
\begin{flushleft}
Si $||e_i|| \neq 1$ (pour \underline{un ou plusieurs $i$}), alors il est possible de transformer la \textbf{base orthogonale} $e$ en une \textbf{base orthonormée}.\\
Ainsi, si $e = (e_1,\cdots ,e_n)$ est une \textbf{base orthogonale}, alors la suite $e' = (e'_1,\cdots ,e'_n)$ définie par
\end{flushleft}
\begin{center}
$e'_i = \dfrac{e_i}{||e_i||}$
\end{center}
est une \textbf{base orthonormée}.

\subsection{En Pratique}
\subsubsection{\emph{Pour déterminer si une base d'un ev est orthogonale}}
Nous avons
\begin{center}
$e = ((1, 0, 0), (0, 1, 0), (0, 0, 1))$\\
$(x|y) = \sum_{i=1}^3 x_i\ y_i$, pour $x, y \in \mathbb{R}^3$.
\end{center}
Pour déterminer si la base $e$ est orthogonale, il faut vérifier, pour tout les éléments de $e$, que $(e_i|e_j) = 0$ ($i \neq j$):
\begin{itemize}
\item $(e_1|e_2)$ = $\sum_{i=1}^3 e_{1i} \cdot e_{2i}$ = $(1 \cdot 0) + (0 \cdot 1) + (0 \cdot 0)$ = $0$
\item $(e_1|e_3)$ = $\sum_{i=1}^3 e_{1i} \cdot e_{3i}$ = $(1 \cdot 0) + (0 \cdot 0) + (0 \cdot 1)$ = $0$
\item $(e_2|e_3)$ = $\sum_{i=1}^3 e_{2i} \cdot e_{3i}$ = $(0 \cdot 0) + (1 \cdot 0) + (0 \cdot 1)$ = $0$
\end{itemize}
Ainsi, $e$ est une base orthogonale.

\subsubsection{\emph{Pour déterminer si une base d'un ev est orthonormée}}
Nous avons
\begin{center}
$e = ((1, 0, 0), (0, 1, 0), (0, 0, 1))$\\
$(x|y) = \sum_{i=1}^3 x_i\ y_i$, pour $x, y \in \mathbb{R}^3$.
\end{center}
Pour déterminer si la base $e$ est orthonormée, il faut:
\begin{enumerate}
\item Vérifier que la base est orthogonale (voir plus haut)
\item Vérifier que, pour tout les éléments de $e$, que $||e_i|| = 1$:
\begin{itemize}
\item $||e_1||$ = $\sqrt{(e_1|e_1)}$ = $\sqrt{\sum_{i=1}^3 e_{1i} \cdot e_{1i}}$ = $\sqrt{(1 \cdot 1) + (0 \cdot 0) + (0 \cdot 0)}$ = $\sqrt{1} = 1$
\item $||e_2||$ = $\sqrt{(e_2|e_2)}$ = $\sqrt{\sum_{i=1}^3 e_{2i} \cdot e_{2i}}$ = $\sqrt{(0 \cdot 0) + (1 \cdot 1) + (0 \cdot 0)}$ = $\sqrt{1} = 1$
\item $||e_3||$ = $\sqrt{(e_3|e_3)}$ = $\sqrt{\sum_{i=1}^3 e_{3i} \cdot e_{3i}}$ = $\sqrt{(0 \cdot 0) + (0 \cdot 0) + (1 \cdot 1)}$ = $\sqrt{1} = 1$
\end{itemize}
\end{enumerate}
Ainsi, $e$ est une base orthonormée.

\subsubsection{\emph{Pour calculer une base orthonormée d'un sev}}
\label{sec:trouver_base_orthonormee}
Nous avons
\begin{center}
$E = \{ (x, y) \in \mathbb{R}^2 | y=0 \}$\\
$(x|y) = \sum_{i=1}^2 x_i y_i \quad$ ($x, y \in \mathbb{R}^2$)
\end{center}
Pour trouver une base orthonormée de $E$, il faut:
\begin{enumerate}
\item Établir une base générique (avec inconnues):\\
Comme, selon les critères du sev $E$, $y=0$, nous pouvons créer une base $e$ ($\in \mathbb{R}^2$) où
\begin{center}
$e = (k, 0) \quad$ ($k \in \mathbb{R}$)
\end{center}
\item Trouver la valeur des inconnues définies au point précédent pour compléter la base:\\
Étant donné qu'une base est orthonormée si $||e|| = 1$, alors:
\begin{center}
$\begin{array}{rcl}
||e|| &=& 1\\
\sqrt{(e|e)} &=& 1\\
\sqrt{(k\cdot k) + (0\cdot 0)} &=& 1\\
\sqrt{k^2} &=& 1\\
k &=& 1
\end{array}$
\end{center}
\end{enumerate}
Ainsi, la base orthonormée de l'espace $E$ est $e = (1, 0)$.

\section{Projections orthogonales}
On parle ici de sous-espaces qui sont orthogonaux entre eux.
\paragraph{Définiton\\}
Soit $E$ un espace euclidien de dimension finie.\\
Pour toute partie $V \subset E$, l'orthogonal $V^{\perp}$ est l'ensemble des vecteurs orthogonaux à tous les vecteurs de $V$:
\begin{center}
$V^{\perp} = (x \in E | (x|v) = 0$ pour tout $v \in V$).
\end{center}
Cet ensemble est un sous-espace vectoriel de $E$.

\paragraph{Proposition\\}
Soit $V$ un sous-espace d'un espace euclidien $E$.\\
Pour tout $x \in E$, il existe un et un seul vecteur $p \in V$ tel que $x-p \in V^{\perp}$.\\
De plus, si $(e_1, \cdots, e_r)$ est une \underline{base orthonormée de $V$}, alors
$$p = \sum_{i=1}^r e_i(e_i|x).$$
\begin{center}
\textit{\underline{En français}: $p$ s'écrit comme la combinaison linéaire de tout les éléments de la base orthonormée avec, pour coefficients, le produit scalaire entre $x$ et l'élément de cette base orthonormé ($e_i$).}
\end{center}
Le vecteur $p$ est appelé \textbf{projection orthogonale du vecteur $x$} sur le sous-espace $V$.
\begin{center}
\includegraphics[scale=0.3]{images/plan_orthogonal.png}
\end{center}
\textbf{EN RÉSUMÉ: La projection orthogonale d'un vecteur $x$ dans un sous-espace $V$ est un point de ce sous-espace\\ tel que $x-p$ n'est pas dans $V$ mais est orthogonal à ce sous-espace\\ c'est-à-dire qu'il est orthogonal à tout les vecteurs de ce sous-espace $V$.}\\\\
\textbf{EN ENCORE PLUS RÉSUMÉ:\\
Le point $p$ se trouve dans $V$ et $x-p$ est orthogonal à tout les vecteurs de $V$.}

\paragraph{Corollaire} Pour tout sous-espace vectoriel $V$ d'un espace euclidien $E$ de dimension finie,
\begin{center}
$E = V \oplus V^{\perp}$.
\end{center}
donc
\begin{center}
$\dim V^{\perp} = \dim E - \dim V$.
\end{center}
De plus,
\begin{center}
$(V^{\perp})^{\perp} = V$.
\end{center}

\subsection{En Pratique}
\subsubsection{\emph{Pour déterminer la projection d'un point sur un sev grâce au produit scalaire}}
\label{sec:calculer_projection}
Nous avons
\begin{center}
$x = (2, 1) \in \mathbb{R}^2$\\
$E = \{ (a, b) \in \mathbb{R}^2 | b=0 \}$\\
$(u|v) = \sum_{i=1}^2 u_i v_i \quad$ ($u, v \in \mathbb{R}^2$)
\end{center}
Pour trouver la projection $p_x$ de $x$ sur $E$, il faut:
\begin{enumerate}
\item Trouver une base orthonormée de $E$ (voir \ref{sec:trouver_base_orthonormee}~\nameref{sec:trouver_base_orthonormee}):
\begin{center}
$e = (1, 0)$ où $||e|| = 1$\\
\end{center}
\item Trouver la projection $p_x$:\\
Selon la théorie (voir plus haut), nous savons que, dans le cas de notre exemple:
\begin{center}
$p_x = (e|x)e$\\
Auquel nous ajoutons $\dfrac{1}{||e||}$ pour ajuster la longueur du vecteur à sa projection.
\end{center}
Ce qui nous donne
\begin{center}
$p_x = \dfrac{(e|x)}{||e||}e$
\end{center}
Ainsi, nous pouvons déterminer que:
\begin{center}
$\begin{array}{rcl}
p_x &=& \dfrac{(e|x)}{||e||}e\\
&=& \dfrac{(1\cdot 2) + (0\cdot 1)}{1}e\\
&=& 2\cdot e\\
&=& 2\cdot (1, 0)\\
&=& (2, 0)\\
\end{array}$
\end{center}
\end{enumerate}
Ainsi, la projection $p_x$ de $x$ sur $E$ est $p_x = (2, 0)$.

\subsubsection{\emph{Pour déterminer la distance entre un point et sa projection}}
Nous avons
\begin{center}
$x = (2, 1) \in \mathbb{R}^2$\\
$E = \{ (a, b) \in \mathbb{R}^2 | b=0 \}$\\
$(u|v) = \sum_{i=1}^2 u_i v_i \quad$ ($u, v \in \mathbb{R}^2$)
\end{center}
Pour trouver la distance entre $x$ et sa projection $p$, il faut:
\begin{enumerate}
\item Calculer $p$ (voir \ref{sec:calculer_projection}~\nameref{sec:calculer_projection}):
\begin{center}
$p = (2, 0)$
\end{center}
\item Calculer la distance $d(x, p)$:\\
La formule pour calculer la distance (euclidienne) entre deux vecteurs est:
\begin{center}
$d(x, p) = \sqrt{\sum_{i=1}^n (x_i - p_i)^2}$,\\où $n$ est le nombre d'élément dans $x$ et dans $p$.\\
(Voir la page Wikipédia \href{https://fr.wikipedia.org/wiki/Distance_(math%C3%A9matiques)#Exemples_de_distances_classiques}{\textit{"Distance (mathématiques)"}} pour plus d'informations)
\end{center}
Ce qui, dans notre cas, nous donne:
\begin{center}
$\begin{array}{rcl}
d(x, p) &=& \sqrt{(x_1 - p_1)^2 + (x_2 - p_2)^2}\\
&=& \sqrt{(2 - 2)^2 + (1 - 0)^2}\\
&=& \sqrt{0 + 1^2}\\
&=& 1\\
\end{array}$
\end{center}
\end{enumerate}
Ainsi, la distance entre $x$ et sa projection $p$ est $d(x, p) = 1$.

\section{Algorithme de Gram-Schmidt}
\paragraph*{Objectif}
La technique des projections orthogonales permet de construire systématiquement une base orthogonale à partir d’une base quelconque.

\subsection{En Pratique}
\subsubsection{\emph{Pour construire une base orthogonale à partir d’une base quelconque avec Gram-Schmidt}}
\begin{enumerate}
\item Choisir un vecteur parmis la base existante:
\begin{center}
$u_1 = e_1$
\end{center}

\item Appliquer la formule $e'_i = \dfrac{e_i}{||e_i||}$ (où $||e_i||$ est la norme de $e_i$) au vecteur $u_1$ établi précédemment pour que la norme de ce vecteur soit égale à 1.\\

\item Pour les autres vecteurs de la base, on les réduits à leurs projections \textbf{orthonormées} par rapport aux vecteurs précédemment calculés:
\begin{center}
$u_2 = e_2 - (e_2|u'_1) u'_1$
\end{center}

Ainsi, on s'arrange pour que le vecteur $u_2$ soit orthonormé par rapport au vecteur $u'_1$,\\

On répète le \emph{2)} pour $u_2$, et on trouve $u'_2$,
\begin{center}
$u_3 = e_3 - (e_3|u'_2) u'_2 - (e_3|u'_1) u'_1$
\end{center}

Ainsi, on s'arrange pour que le vecteur $u_3$ soit orthonormé par rapport aux vecteurs $u'_1$ et $u'_2$.
\end{enumerate}

\newpage
