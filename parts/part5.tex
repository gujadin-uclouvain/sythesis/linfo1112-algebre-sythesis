%----------------------------------------------------------------------------------------
%	PART 5
%----------------------------------------------------------------------------------------
\lhead{OPÉRATEURS LINÉAIRES}
\part{Opérateurs linéaires}
Un opérateur linéaire est une application linéaire d'un ev dans lui-même.\\

On peut donc considérer un opérateur linéaire, sur un ev, comme une re-transformation de cet espace, et envisager de trouver:
\begin{itemize}
\item les vecteurs fixes sous cette transformation, ou,
\item les vecteurs transformés en leurs opposés, ou,
\item Plus généralement les vecteurs dont l'image est un multiple d'eux-mêmes, appelés \emph{vecteurs propres} de l'opérateur.\\
\end{itemize}

\section{Notions fondamentales}
\subsection{Valeurs propres et vecteurs propres}
\begin{itemize}
\item[•] Soit $A : E \rightarrow E$, un opérateur linéaire sur un ev $E$, sur un corps arbitraire $\mathbb{K}$.\\
Un \emph{vecteur propre} de $A$ est un vecteur $v \in E$ \textit{non nul} tel que
\begin{center}
$A(v) = v\lambda\quad$ pour un certain $\lambda \in \mathbb{K}$.\\
\textit{\underline{En français:} L'opérateur $A$ appliqué au vecteur $v$ permet d'obtenir une valeur proportionnelle à $v$, à une constante près, où cette constante est la valeur propre $\lambda$.}
\linebreak
\end{center}

\item[•] Le scalaire propre $\lambda$ est appelé \emph{valeur propre} de $A$ associée à $v$; on dit aussi que $v$ est un vecteur propre de $A$ de valeur propre $\lambda$.\\

\item[•] L'équation $A(v) = v\lambda$ peut aussi s'écrire
\begin{center}
$\lambda I_E(v) - A(v) = (\lambda I_E - A)(v) = 0$,
\end{center}
ainsi, \textbf{le vecteur propre $v$ est dans le noyau de $\lambda I_E - A$}.\\
\end{itemize}
\textbf{NB:} Comme nous sommes limité aux nombres réels $\mathbb{R}$ dans ce cours, certains opérateurs linéaires peuvent ne pas avoir de valeur propre, ni vecteur propre.

\subsection{Espaces propres}
\begin{itemize}
\item[•] Donc, pour tout $\lambda \in \mathbb{K}$, on appelle \textit{espace propre de valeur propre $\lambda$} le sous-espace défini par
\begin{center}
$E(\lambda) = Ker(\lambda I_E - A)$\\
\textit{\underline{En français:} l'espace des vecteurs propres $E$ associé à cette valeur propre $\lambda$ est le noyau de l'opérateur $\lambda I_E - A$.}
\linebreak
\end{center}

\item[•] Si on a des solutions non nulles au système d'équation $(\lambda I_E - A)(v) = 0$, il faut alors que:
\begin{center}
$\det(\lambda I_E - A) = 0$
\end{center}
\end{itemize}

\subsection{Polynôme caractéristique}
\begin{itemize}
\item[•] L'on appelle \textit{polynôme caractéristique}:
\begin{center}
$Pc_A(X) = \det(X I_E - A)$.\\
\textit{\underline{En français:} le déterminant de $\lambda I_E - A$ où $\lambda$ est remplacé par $X$ (Il s'agit juste d'une question de notation).}
\linebreak
\end{center}

\item[] Ce polynôme est unitaire, c'est-à-dire que le coefficient du terme de plus haut degré est 1, et son degré est égal à la dimension de $E$:
\begin{center}
$\deg Pc_A = \dim E$
\linebreak
\end{center}

\item[•] Notons qu'il existe 2 expressions pour calculer le polynôme caractéristique:
\begin{center}
$\begin{array}{lcl}
1.\ Pc_A &=& \det(A - \lambda I_n)\\
2.\ Pc_A &=& \det(\lambda I_n - A)$\textbf{ voir |*|}$
\end{array}$
\end{center}
Ces deux expressions présentent chacunes des avantages et des inconvéniants:
\begin{enumerate}
\item $Pc_A = \det(A - \lambda I_n)$:
\begin{itemize}
\item[+] L'équation $Pc_A (0) = \det(A)$ est toujours vrai
\item[--] Le polynôme caractéristique n'est pas unitaire
\end{itemize}

\item $Pc_A = \det(\lambda I_n - A)$:
\begin{itemize}
\item[+] Le polynôme caractéristique est unitaire
\item[--] Lorsque la matrice $A$ est d'ordre impair et $\det(A) \neq 0 \quad \Rightarrow \quad Pc_A (0) \neq \det(A)$
\end{itemize}
\end{enumerate}
\textbf{|*| \underline{Dès lors, pour obtenir une égalité entre ces deux expressions, il faut que}}
\begin{large}
\begin{center}
$\det(A - \lambda I_n) = \underline{(-1)^n} \cdot \det(\lambda I_n - A)$
\end{center}
\end{large}
\end{itemize}

\subsection{En Pratique}
\subsubsection{\emph{Pour déterminer le polynôme caractéristique à partir d'une matrice}}
\label{subsec:calc-poly-car}
\begin{enumerate}
\item[] Si $A$ =
$\begin{pmatrix}
3 & 2\\
3 & -2
\end{pmatrix}$
, il faut:
\item Trouver la matrice $B (= \lambda I - A)$, où $Pc_A = p(\lambda) = det(B)$:
\begin{center}
$\lambda I - A =
\lambda \cdot
\begin{pmatrix}
1 & 0\\
0 & 1
\end{pmatrix}
+
\begin{pmatrix}
-3 & -2\\
-3 & 2
\end{pmatrix}
=
\begin{pmatrix}
\lambda-3 & -2\\
-3 & \lambda+2
\end{pmatrix}$
\end{center}
\item Calculer le polynôme caractéristique $Pc_A (= p(\lambda))$ (le déterminant de la matrice $B$ trouvée en 1.):
\begin{center}
$\begin{array}{rccll}
\Delta &=& (\lambda - 3) \cdot (\lambda + 2) - 6\\
\Delta &=& (-1)^2 \cdot (\lambda^2 -\lambda - 12)\\
(-1)^2 \cdot (\lambda^2 -\lambda - 12) &=& \underline{\lambda^2 -\lambda - 12} &=& 0\\
\end{array}$
\end{center}
\end{enumerate}

Ainsi, $\quad \hyperref[sec:det-base]{Pc_A = \lambda^2 -\lambda - 12} \quad \Leftrightarrow \quad$ \underline{POLYNÔME CARACTÉRISTIQUE}.\\

\subparagraph*{Pour aller plus loin} Nous pouvons prendre de l'avance sur la sous-section \ref{subsec:calc-vp} en trouvant les valeurs propres (les racines) de ce polynôme caractéristique:\\
\begin{enumerate}
\item[3.] Calculer les valeurs propres ($\lambda$):
\begin{center}
$\lambda = \dfrac{1 \pm \sqrt[]{1+48}}{2}$\\
$\begin{array}{ccc}
\lambda_1 = 4 && \lambda_2 = -3
\end{array}$
\end{center}
\end{enumerate}

Ainsi, $\quad \lambda =
\{4, -3\}
\quad \Leftrightarrow \quad$ \underline{VALEURS PROPRES}.

\subsubsection{\emph{Pour déterminer  les valeurs propres et vecteurs propres d'une matrice}}
\label{subsec:calc-vp}
\begin{enumerate}
\item[] Si $A$ =
$\begin{pmatrix}
1 & 0\\
0 & 2
\end{pmatrix}$
, il faut:

\item Calculer le polynôme caractéristique $Pc_A (= p(\lambda))$ (voir~\ref{subsec:calc-poly-car} \nameref{subsec:calc-poly-car}):
\begin{center}
$p(\lambda) = (1-\lambda)\cdot(2-\lambda)$
\end{center}

\item Calculer les racines de $p(\lambda)$:
\item[] (= aux \underline{VALEURS PROPRES} de $A$($\lambda_i$))
\begin{center}
On trouve que $\lambda_1 = 1$ et $\lambda_2 = 2 \quad$ car $p(0) = (1-0) . (2-0)$
\end{center}

\item Pour chaque valeur propre trouvée en 3., trouver les valeurs de $x$ tel que \underline{$Ax = \lambda x$} [$\Leftrightarrow (A - \lambda I)\cdot x = 0$]: (où $\lambda$ est la valeur propre)
\item[] (= aux \underline{VECTEURS PROPRES} de $A$($x_{\lambda_i}$))
\item[] \textbf{\underline{[!!! Le système DOIT être AU MOINS une fois indéterminé]}}
\begin{flushleft}
\begin{itemize}
\item[] Si $x \in \mathbb{R}^2$, $x =
\begin{pmatrix}
a\\
b
\end{pmatrix}$\\
\item[•] \underline{Racine 1 $\rightarrow \quad \lambda = 1$}:
\begin{center}
$\begin{array}{ccccl}
A&\cdot &\ x &=&\ 1x\\
\begin{pmatrix}
1 & 0\\
0 & 2
\end{pmatrix}
&\cdot &
\begin{pmatrix}
a\\
b
\end{pmatrix}
 &=&
\begin{pmatrix}
a\\
b
\end{pmatrix}.
\end{array}$
\end{center}
$\Leftrightarrow$
$\left\{
\begin{array}{rl}
a\ =\ a\ &\rightarrow\ INDÉTERMINATION\\
2b\ =\ b\ &\rightarrow\ b = 0
\end{array}
\right.$\\
$E(1) = \lbrace (a,0)| a \in \mathbb{R}\rbrace = sev\langle(1,0)\rangle\quad \Rightarrow \quad x_{\lambda_1} =
\begin{pmatrix}
1\\
0
\end{pmatrix}$\\
\item[•] \underline{Racine 2 $\rightarrow \quad \lambda = 2$}:
\begin{center}
$\begin{array}{ccccl}
A&\cdot &\ x &=&\ \ 2x\\
\begin{pmatrix}
1 & 0\\
0 & 2
\end{pmatrix}
&\cdot &
\begin{pmatrix}
a\\
b
\end{pmatrix}
 &=&
\begin{pmatrix}
2a\\
2b
\end{pmatrix}.
\end{array}$
\end{center}
$\Leftrightarrow$
$\left\{
\begin{array}{rl}
a\ =\ 2a\ &\rightarrow\ a = 0\\
2b\ =\ 2b\ &\rightarrow\ INDÉTERMINATION
\end{array}
\right.$\\
$E(2) = \lbrace (0,b)| b \in \mathbb{R}\rbrace = sev\langle(0,1)\rangle\quad \Rightarrow \quad x_{\lambda_2} =
\begin{pmatrix}
0\\
1
\end{pmatrix}$
\item[\textbf{NB:}] Le calcul des valeurs de x au 4. peut aussi être réalisé grâce à une élimination de \hyperref[sec:gauss-jordan]{Gauss-Jordan}.
\end{itemize}
\end{flushleft}
\item Finalement, on peut noter les valeurs et vecteurs propres trouvés de cette façon:
\begin{center}
$\begin{array}{rcl}
\lambda &=& \{1, 2\}\\
E(1) &=& sev\langle(1, 0)\rangle\\
E(2) &=& sev\langle(0, 1)\rangle
\end{array}$
\end{center}
\end{enumerate}

\textbf{Remarques:}
\begin{itemize}
\item Imaginons un système à valeur propre $\lambda$ où:
\begin{flushleft}
$\Leftrightarrow$
$\left\{
\begin{array}{ll}
-2x + y + z = 0\\
x - 2y + z = 0\\
x + y - 2z = 0
\end{array}
\right.$\\
$\Leftrightarrow$
$\left\{
\begin{array}{ll}
a\ =\ \alpha\ |\ \alpha \in \mathbb{R}\ &\rightarrow\ INDÉTERMINATION\\
b\ =\ \alpha\\
c\ =\ \alpha
\end{array}
\right.$
\end{flushleft}
\item[] Dans ce cas, on remarque que $a = b = c$ mais que le degré d'indétermination du système vaut 1 car il y a 1 ligne qui est combinaison des 2 autres.
\item[] Ainsi, on a que:
\begin{center}
$E(\lambda) = \lbrace (\alpha,\alpha, \alpha)| \alpha \in \mathbb{R}\rbrace = sev\langle(1,1,1)\rangle$
\end{center}
\item Le nombre de vecteurs propres engendré par leur valeur propre doit correspondre avec le degré d'indétermination du système:
\item[] Imaginons un système à valeur propre $\lambda$ où:
\begin{flushleft}
$\Leftrightarrow$
$\left\{
\begin{array}{ll}
a\ =\ \alpha\ |\ \alpha \in \mathbb{R}\ &\rightarrow\ INDÉTERMINATION\\
b\ =\ \beta\ |\ \alpha \in \mathbb{R}\ &\rightarrow\ INDÉTERMINATION\\
c\ =\ \alpha + \beta
\end{array}
\right.$
\end{flushleft}
\item[] Dans ce cas, le degré d'indétermination du système vaut 2.
\item[] Ainsi, on a que:
\begin{center}
$E(\lambda) = \lbrace (\alpha,\beta, \alpha + \beta)| \alpha, \beta \in \mathbb{R}\rbrace = sev\langle(1,1,2),(0,1,1)\rangle$
\end{center}
\item[] où les vecteurs propres
$\begin{pmatrix}
1\\
1\\
2
\end{pmatrix}$
et
$\begin{pmatrix}
0\\
1\\
1
\end{pmatrix}$
sont \textbf{\underline{linéairement indépendants}} car il s'agit d'une \textbf{base} du système.
\end{itemize}
\subsubsection{\emph{Pour déterminer si une matrice est inversible en connaissant ses valeurs propres}}
\begin{center}
Si $0$ est valeur propre de $A$\\
$\Leftrightarrow$\\
$0$ est racine du polynôme caractéristique\\
$\Leftrightarrow$\\
\hyperref[sec:inverse]{$det(A) = 0$}\\
$\Leftrightarrow$\\
$A$ N'EST PAS INVERSIBLE !!!
\end{center}

\section{Diagonalisation}
\label{sec:diagonalisation}
$A$ désigne un opérateur linéaire sur un espace $E$ de dimension finie sur un corps $\mathbb{K}$.\\

On dit que l'opérateur $A$ est \emph{diagonalisable} s'il existe une base $e$ de $E$ par rapport à laquelle la matrice ${}_e(A)_e$ est \hyperref[sec:type-matrice]{diagonale}.\\

L'objectif principal de la diagonalisation est de pouvoir écrire une matrice carrée $A$ ($\in \mathbb{R}^{n\times n}$) sous la forme
\begin{center}
$A = P \cdot \Lambda \cdot P^{-1}$\\
où $\Lambda$ est une \hyperref[sec:type-matrice]{matrice diagonale}.
\end{center}
L'une des utilisation de cette forme est de pouvoir calculer la puissance d'une matrice, c'est-à-dire:
\begin{center}
$A^k = P \cdot \Lambda^k \cdot P^{-1}$
\end{center}
\paragraph{Théorème Spectral:}
Pour une matrice réelle $A \in \mathbb{R}^{n\times n}$, les conditions suivantes sont \textbf{équivalentes}:
\begin{enumerate}
\item[(a)] Il existe une \hyperref[sec:type-matrice]{matrice orthogonale} $Q \in \mathbb{R}^{n\times n}$ telle que $Q^{-1} \cdot A \cdot Q = \Lambda$ soit une matrice diagonale.

\item[(b)] Il existe une base orthonormée (par rapport au produit scalaire usuel) de $\mathbb{R}^n$.\\ La base est constituée des \underline{vecteurs propres} de $A$.\\
\textbf{Cette condition sera la plus utilisée dans ce cours}

\item[(c)] La matrice $A$ est \hyperref[sec:type-matrice]{symétrique}.
\end{enumerate}

Si l'un de ces trois points est vrai, alors, la matrice $A$ est diagonalisable.

De plus, la matrice $\Lambda$ peut être trouvée soit:
\begin{itemize}
\item Grâce à la propriété (a) où $D = diag(\lambda_i)$.
\item En construisant une matrice diagonale composée des valeurs propres de $A$.
\end{itemize}
\paragraph{Règle 1}
D'après le théorème spectral ci-dessus,

\emph{Une \hyperref[sec:type-matrice]{matrice symétrique} est toujours diagonalisable}
\paragraph{Règle 2\\}
On définit:
\begin{itemize}
\item La matrice $P$ comme la matrice dont les colonnes sont les VECTEURS PROPRES.
\item La matrice $\Lambda$ où la diagonale principale est composée des VALEURS PROPRES.
\item[] (Les vecteurs propres dans $P$ doivent être dans le MÊME ORDRE que les valeurs propres dans $\Lambda$.
\item A est la matrice de départ.\\
\end{itemize}
On a:
\begin{center}
$A \cdot P = P \cdot \Lambda$
\end{center}
On peut déduire que:
\begin{center}
$A = P \cdot \Lambda \cdot P^{-1}$\\
ou\\
$\Lambda = P^{-1} \cdot A \cdot P$
\end{center}

\subsection{Multiplicités des valeurs propres}
\label{sec:mult-vp}
Le point (b) du \hyperref[sec:diagonalisation]{théorème spectral} sur la diagonalisation nous énonce qu'il existe une base de $\mathbb{R}^n$ composée de $n$ vecteurs propres de $A$.\\

Pour appliquer cela, il faut utiliser la condition sur les multiplicités des valeurs propres énonce que
\begin{center}
$0 \leq m_g(\lambda_i) \leq m_a(\lambda_i) \leq n$
\end{center}

où:
\begin{itemize}
\item[] \underline{$m_g(\lambda_i)$}, \textbf{la multiplicité géométrique}, est la dimension de l'espace propre associée à une valeur propre $\lambda_i$\\
(= nombre de vecteurs propres, dans la base de l'espace propre, associés à $\lambda_i$)\\
(= degrés d'indétermination de $Ax = \lambda x$)\\

\item[] \underline{$m_a(\lambda_i)$}, \textbf{la multiplicité algébrique}, est le nombre de fois qu'une valeur propre $\lambda_i$ apparaît dans le polynôme caractéristique\\
(= nombre de valeurs propres $\lambda_i$ associées au polynôme caractéristique)\\

\item[] \underline{$n$}, \textbf{la dimension de A} ($\dim A$).\\
\end{itemize}

Pour que les vecteurs propres forment une base de $\mathbb{R}^n$, il faut que la matrice $A$ soit de \hyperref[sec:type-matrice]{plein rang}, et donc que:
\begin{itemize}
\item Chaque valeur propre doit être associée à un vecteur propre,
\item Une valeur propre double, triple ou quadruple doit avoir respectivement 2, 3, ou 4 vecteurs propres associés.
\end{itemize}

Cela nous permet d'arriver à la conclusion que
\begin{center}
$m_g(\lambda_i) = m_a(\lambda_i)$,
\end{center}
qui permet finalement de dire que $A$ est \underline{DIAGONALISABLE}.

\subsection{En Pratique}
\subsubsection{\emph{Pour vérifier si une matrice est diagonalisable dans $\mathbb{R}^n$}}
\label{sec:is-diagonalisable}
Nous allons utiliser la deuxième propriété du \hyperref[sec:diagonalisation]{théorème spectral}\\\\

Si $A =
\begin{pmatrix}
4 & -1 & 2\\
0 & 5 & 0\\
0 & 0 & 5
\end{pmatrix}$, il faut:
\begin{enumerate}
\item Calculer les valeurs propres et les vecteurs propres (voir ~\ref{subsec:calc-vp} \nameref{subsec:calc-vp}):
\begin{center}
$\begin{array}{rcl}
\lambda &=& \{4, 5, 5\}\\
E(4) &=& sev\langle(1, 0, 0)\rangle\\
E(5) &=& sev\langle(-1, 1, 0),(2, 0, 1)\rangle
\end{array}$
\end{center}

\item Vérifier que chaque valeur propre $\in \mathbb{R}$:
\begin{center}
Vrai car énoncé dans le titre.
\end{center}

\item Vérifier la \hyperref[sec:mult-vp]{condition sur les multiplicités}:
\begin{itemize}
\item $m_a(4) = 1 = m_g(4)$:
Vrai car la valeur propre $4$:
\begin{enumerate}
\item Apparaît 1 seule fois dans le polynôme caractéristique,
\item A bien 1 seul vecteur de base.
\end{enumerate}
\item $m_a(5) = 2 = m_g(5)$:
Vrai car la valeur propre $5$:
\begin{enumerate}
\item Apparaît 2 fois dans le polynôme caractéristique,
\item A bien 2 vecteurs de base.
\end{enumerate}
\end{itemize}

\item Vérifier si les vecteurs propres forment bien une base de $\mathbb{R}^3$ (car 3 vecteurs propres):
\begin{enumerate}
\item[] Il faut stocker les vecteurs propres dans une matrice
\begin{center}
$P =
\begin{bmatrix}
1 & -1 & 2\\
0 & 1 & 0\\
0 & 0 & 1
\end{bmatrix}$
\end{center}
\item[] L'on remarque que la matrice est \hyperref[sec:type-matrice]{triangulaire supérieure} (donc déjà échelonnée).
\item[] Ce qui permet de déterminer que:
\begin{center}
rang$(P) = 3$\\
$\Downarrow$\\
Les vecteurs propres sont linéairement indépendants\\
$\Downarrow$\\
Ils forment une base de $\mathbb{R}^3$
\end{center}
\end{enumerate}
\item Vérifier votre réponse, grâce à la formule:
\begin{center}
$A\cdot P = P \cdot D$
\end{center}
\item La matrice $A$ est diagonalisable.
\end{enumerate}

\subsubsection{\emph{Pour exprimer une matrice diagonalisable $A$ sous la forme $P \cdot D \cdot P^{-1}$}}
Si $A =
\begin{pmatrix}
4 & -1 & 2\\
0 & 5 & 0\\
0 & 0 & 5
\end{pmatrix}$, il faut:
\begin{enumerate}
\item \hyperref[sec:is-diagonalisable]{Vérifier si la matrice $A$ est diagonalisable} (les valeurs utilisées dans cet exemple sont celles trouvées au point précédent)

\item Trouver \hyperref[sec:inverse]{$P^{-1}$} (où $P$ est la matrice des vecteurs propres):
\begin{center}
$P^{-1} =
\begin{bmatrix}
1 & 1 & -2\\
0 & 1 & 0\\
0 & 0 & 1
\end{bmatrix}$
\end{center}

\item Trouver $D$ (qui est la matrice diagonale des valeurs propres associées aux vecteurs propres):
\begin{center}
$D =
\begin{bmatrix}
4 & 0 & 0\\
0 & 5 & 0\\
0 & 0 & 5
\end{bmatrix}$
\end{center}
\end{enumerate}
