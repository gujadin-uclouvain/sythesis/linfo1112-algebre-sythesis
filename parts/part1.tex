%----------------------------------------------------------------------------------------
%	PART 1
%----------------------------------------------------------------------------------------
\pagestyle{fancy}\fancyhf{}\lhead{SYSTÈMES LINÉAIRES, MATRICES ET DÉTERMINANTS}\rhead{ALGÈBRE}\cfoot{\thepage}
\part{Systèmes linéaires, matrices et\\ déterminants}
\section{Rappels}
\subsection{Système Trivial}
\label{sec:trivial}
En mathématiques, on qualifie de trivial un énoncé dont on juge la vérité évidente à la lecture, ou encore un objet mathématique dont on estime que l'existence va de soi et que son étude n'a pas d'intérêt:\\
Exemple: $x = y$ est un système trivial.

\subsection{Méthode d'Horner}
La méthode d'Horner est un procédé permettant de factoriser un polynôme en un produit de polynômes de degré plus petit.\\
Utile pour trouver les racines d'une matrice ayant un déterminant sous forme de polynôme cubique.\\\\
Il se réalise de la manière suivante:\\
Imaginons un polynôme p($\lambda$) = $-\lambda^{3}\ -\ 3\lambda^{2}\ +\ 4$\\
L'on peut deviner, par tâtonnements, que $\lambda$ = 1 est une racine car:
\begin{align*}
p(1) &=\ -1\ -\ 3\ +\ 4\\
&=\ 0
\end{align*}
Ensuite, dans un tableau, l'on ajoute:
\begin{itemize}
\item À gauche, la racine (horner).\\
\item En haut, les termes de l'équation.\\
\item En bas, l'addition (en mauve) des termes et la multiplication d'horner avec les solutions trouvées (en rouge) ainsi que le report de ces valeurs sur la section suivante (en rouge flèché).
\end{itemize}
\begin{center}
\includegraphics[scale=0.5]{images/horner.png}\\
\end{center}
Ainsi, nous obtenons:
\begin{align*}
p(\lambda)\ &=\ (\lambda\ -\ 1).(-\lambda^{2}\ -\ 4\lambda\ -\ 4)\\
&=\ -(\lambda\ -\ 1).(\lambda^{2}\ +\ 4\lambda\ +\ 4)\\
&=\ -(\lambda\ -\ 1).(\lambda\ +\ 2)^{2}
\end{align*}
$\Rightarrow$ racines : $\lbrace -2, -2, 1\rbrace$\\

\section{Notions fondamentales}
\subsection{Matrices}
\label{sec:coefficients-sys}
La matrice représente un système d'équation de ce style:
\begin{center}
$\left\{
\begin{array}{rl}
a_{11} x_1 \ +\ \cdots \ +\ a_{1n} x_n \quad & = \quad b_1 \\
&\ $\vdots$ \\
a_{m1} x_1 \ +\ \cdots \ +\ a_{mn} x_n \quad &= \quad b_m
\end{array}
\right.$
\end{center}
en un tableau à m lignes et n colonnes où chaque ligne correspond à une équation du système. Nous avons:
\begin{center}
$\begin{pmatrix}
a_{11} & \cdots & a_{1n}\\
\vdots & & \vdots\\
a_{m1} & \cdots & a_{mn}
\end{pmatrix}$
\end{center}
qui représente la \textit{matrice des coefficients du système}, et
\begin{center}
$\begin{pmatrix}
b_1\\
\vdots\\
b_m
\end{pmatrix}$
\end{center}
qui représente la \textit{colonne des termes indépendants}.

\subsubsection{Matrices Complètes}
\label{sec:matrice-complete}
\begin{center}
$\begin{pmatrix}
\begin{array}{lcc|c}
a_{11} & \cdots & a_{1n} & b_1\\
\vdots & & \vdots & \vdots\\
a_{m1} & \cdots & a_{mn} & b_m
\end{array}
\end{pmatrix}$
\end{center}
Ce tableau est appelée matrice complète du système car l'on a ajouté à la matrice des coefficients du système, la colonne des termes indépendants ($b_{1}$, $\cdots$, $b_{m}$).

\subsection{Système Homogène}
Un système d'équations algébriques linéaires est dit homogène si les seconds membres $b_{1},\cdots ,b_{m}$ sont tous nuls:
\begin{center}
$\left\{
\begin{array}{rcl}
a_{11} x_1 \ +\ \cdots \ +\ a_{1n} x_n \quad &=& \quad 0 \\
&\vdots&\\
a_{m1} x_1 \ +\ \cdots \ +\ a_{mn} x_n \quad &=& \quad 0
\end{array}
\right.$
\end{center}

\subsection{Types de Matrices}
\label{sec:type-matrice}
\textit{Cette sous-section rassemble les différents types de matrices présents dans cette synthèse.\\
Ainsi, il est possible que certaines défnitions ne soient pas claires si vous n'avez pas encore abordé les sections suivantes.}\\
\begin{itemize}
\item Les matrices qui n'ont qu'une seule ligne sont appelées \textbf{matrices lignes}.\\
\item Les matrices qui n'ont qu'une seule colonne sont appelées \textbf{matrices colonnes}.\\
\item Les matrices dont le nombre de lignes = nombre de colonnes sont appelées \textbf{matrices carrées} (noté $A \in \mathbb{K}^{n\times n}$ où $n$ est le nombre de lignes/colonnes).\\
\item \textbf{L'ordre d'une matrice carrée} est le nombre de ses lignes ou de ses colonnes.\\
\item \textbf{La diagonale principale d'une matrice carrée} est formée des entrées dont les indices sont égaux:\\
Exemple:
$\begin{pmatrix}
1 & 0 & 5 & 8 \\
2 & 1 & 7 & 6 \\
7 & 8 & 1 & 0 \\
0 & 9 & 4 & 1
\end{pmatrix}$
Où les 1 représentent la diagonale principale.\\
\item Une \textbf{matrice diagonale} est une matrice carrée dont toutes les entrées, hors de la diagonale principale, sont nulles:\\
Exemple:
$\begin{pmatrix}
1 & 0 & 0 & 0 \\
0 & 5 & 0 & 0 \\
0 & 0 & 2 & 0 \\
0 & 0 & 0 & 8
\end{pmatrix}$\\
\item La \textbf{matrice nulle} est la matrice où toutes les entrées sont nulles.\\
\item La \textbf{matrice unité} (ou matrice identité) est représenté comme suit :\\
$\begin{pmatrix}
1 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{pmatrix}$
Où les $1$ représentent la diagonale principale.\\\\
Elle est défini par:
\begin{flushright}
$I_n\ =\ (\delta_{ij})_{1 \geq i,j \geq n}$
\end{flushright}
où
\begin{flushright}
$\delta_{ij}\ =\ $
$\left\{
\begin{array}{rl}
0\ si\ i\neq\ j\\
1\ si\ i = \ j
\end{array}
\right.$
\end{flushright}
La fonction $\delta_{ij}$ ainsi définie est appelée \emph{symbole de Kronecker}.\\
\item Une \textbf{matrice triangulaire supérieure} (resp. inférieure) est une matrice carrée dont les valeurs sous (resp. au-dessus) la diagonale principale sont nulles:\\
Exemple:
$\begin{pmatrix}
1 & 9 & 1 & 3 \\
0 & 5 & 2 & 7 \\
0 & 0 & 2 & 4 \\
0 & 0 & 0 & 8
\end{pmatrix}$\\
\item Une \textbf{matrice symétrique}
est une matrice carrée qui est égale à sa \hyperref[sec:transpose]{transposée}, c'est-à-dire:
\begin{center}
$A^t$ $= A$.
\end{center}
\item Une \textbf{matrice élémentaire} (voir sous-section~\ref{sec:matrice-elem})\\
\item Une \textbf{matrices carrée régulière} (voir sous-section~\ref{sec:regulier})\\
\item Une \textbf{matrice orthogonale} est une matrice unitaire \hyperref[sec:inverse]{inversible}, à coefficients dans $\mathbb{R}$, où son \hyperref[sec:inverse]{inverse} est égal à sa \hyperref[sec:transpose]{transposée}, c'est-à-dire:
\begin{center}
$A \in \mathbb{R}^{n\times n}$, $A^{-1} = A^t$
\end{center}
\item Une \textbf{matrice de plein rang} est une matrice carrée où son nombre de lignes est égal à son \hyperref[sec:rang-matrice]{rang}.
\end{itemize}

\subsection{Opérations élémentaires sur les matrices}
Il existe 3 types d'opérations sur les équations d'un système (et donc sur les matrices):\\

\begin{itemize}
\item[\textbf{I.}] Dans un système d'équations, on peut remplacer une des équations par la somme de celle-ci et d'une multiplication d'une autre équation du système sans modifier l'ensemble des solutions.

\item[\textbf{II.}] Dans un système d'équations, on peut échanger deux équations sans modifier l'ensemble des solutions.

\item[\textbf{III.}] Dans un système d'équations, on peut multiplier les deux membres d'une équations par un élément NON NUL du corps de base de $\mathbb{K}$ sans modifier l'ensemble des solutions.
\end{itemize}

\subsection{Échelonnement}
Une matrice ayant comme propriétés:
\begin{itemize}
\item Un nombre d'entrées nulles au début de chaque ligne (sauf éventuellement la 1ère),
\item qui augmente à chaque ligne,
\item avec une augmentation stricte (d'une différence de 1) pour les lignes avant une ligne non-nulle;
\end{itemize}

est appelée \textbf{matrice à ligne échelonnées}.\\

Dans une ligne non nulle, la première entrée non nulle est appelée \textbf{pivot} de la ligne. Ainsi, une matrice à lignes échelonnées prend cette forme:
\begin{center}
$(a_{ij})_{
\begin{matrix}
1 \geq i \geq 5\\
1 \geq j \geq 6
\end{matrix}
}\ =\ $
$\begin{pmatrix}
0 & 1 & 2 & 3 & 4 & 5\\
0 & 0 & 0 & 6 & 7 & 8\\
0 & 0 & 0 & 0 & 9 & 10\\
0 & 0 & 0 & 0 & 0 & 0\\
0 & 0 & 0 & 0 & 0 & 0
\end{pmatrix}$\\
Les pivots ici sont $a_{12}$ = 1, $a_{24}$ = 6 et $a_{35}$ = 9.\\
\end{center}

En revanche, la matrice suivante n'est pas à ligne échelonnées:
\begin{center}
$\begin{pmatrix}
0 & 0 & 1 & 2\\
0 & 0 & 0 & 3\\
0 & 0 & 0 & 4\\
0 & 0 & 0 & 0
\end{pmatrix}$
\end{center}

\subsubsection{Forme Réduite de Gauss-Jordan}
\label{sec:gauss-jordan}
Une \textbf{matrice sous forme réduite de Gauss-Jordan} est une matrice à lignes échelonnées ayant les valeurs au-dessus des pivots établient à 0 (où les "$*$" sont des réels quelconques) :
\begin{center}
$\begin{pmatrix}
\begin{array}{cccccccccccccc}
0 & \cdots & 0 & 1 & * & \cdots & * & 0 & * & \cdots & * & 0 & * & \cdots\\
0 & \cdots & 0 & 0 & 0 & \cdots & 0 & 1 & * & \cdots & * & 0 & * & \cdots\\
0 & \cdots & 0 & 0 & 0 & \cdots & 0 & 0 & 0 & \cdots & 0 & 1 & * & \cdots\\
 &  &  &  &  &  &  & \cdots &  &  &  &  &  &
\end{array}
\end{pmatrix}$
\end{center}

\subsection{Addition de Matrices}
Pour additionner deux matrices entre elles, il faut que respectivement le nombre de lignes et de colonnes de la matrice A = nombre de lignes et de colonnes de la matrice B.\\\\
La résolution se fait comme suit:\\
Si A =
$\begin{pmatrix}
1 & 3\\
1 & 0\\
1 & 2
\end{pmatrix}$
et B =
$\begin{pmatrix}
0 & 0\\
7 & 5\\
2 & 1
\end{pmatrix}$\\
Alors,\\
A . B =
$\begin{pmatrix}
1 + 0 & 3 + 0\\
1 + 7 & 0 + 5\\
1 + 2 & 2 + 1
\end{pmatrix}$
=
$\begin{pmatrix}
1 & 3\\
8 & 5\\
3 & 3
\end{pmatrix}$

\subsubsection{Somme directe}
\label{somme_directe}
Pour toutes matrices quelconques $A$ (de taille $m\times n$) et $B$ (de taille $p\times q$), il existe la \textbf{somme directe} de $A$ et $B$, notée $A\bigoplus B$ et définie par:
\begin{center}
$A\bigoplus B =
\begin{pmatrix}
a_{11} & \cdots & a_{1n} & 0 & \cdots & 0\\
\vdots & \cdots & \vdots & \vdots & \cdots & \vdots\\
a_{m1} & \cdots & a_{mn} & 0 & \cdots & 0\\
0 & \cdots & 0 & b_{11} & \cdots & b_{1q}\\
\vdots & \cdots & \vdots & \vdots & \cdots & \vdots\\
0 & \cdots & 0 & b_{p1} & \cdots & b_{pq}
\end{pmatrix}$
\end{center}
Exemple:
\begin{center}
$\begin{pmatrix}
1 & 3 & 2\\
2 & 3 & 1
\end{pmatrix}$
$\bigoplus$
$\begin{pmatrix}
1 & 6\\
8 & 1
\end{pmatrix}$
 =
$\begin{pmatrix}
1 & 3 & 2 & 0 & 0\\
2 & 3 & 1 & 0 & 0\\
0 & 0 & 0 & 1 & 6\\
0 & 0 & 0 & 8 & 1
\end{pmatrix}$
\end{center}

\subsection{Multiplication de Matrices}
Pour multiplier deux matrices entre elles, il faut que le nombre de ligne de la matrice A = nombre de colonnes de la matrice B.\\\\
La résolution se fait comme suit:\\
Si A =
$\begin{pmatrix}
a & b\\
c & d
\end{pmatrix}$
et B =
$\begin{pmatrix}
e & f\\
g & h
\end{pmatrix}$\\
Alors,\\
A . B =
$\begin{pmatrix}
(a \cdot e + b \cdot g) & (a \cdot f + b \cdot h)\\
(c \cdot e + d \cdot g) & (c \cdot f + d \cdot h)
\end{pmatrix}$\\\\
PS: Le produit des matrices N'EST PAS commutatif : AB $\neq$ BA\\
--------------------------------------------------------------------------------------------------\\
La mutiplication matricielle peut s'écrire  plus simplement. En effet, le système linéaire de m équations à n inconnues:
\begin{center}
$\left\{
\begin{array}{rl}
a_{11} x_1 \ +\ \cdots \ +\ a_{1n} x_n \quad & = \quad b_1 \\
&\ $\vdots$ \\
a_{m1} x_1 \ +\ \cdots \ +\ a_{mn} x_n \quad &= \quad b_m
\end{array}
\right.$
\end{center}
s'écrit simplement $\Rightarrow$  A $\cdot$ X = B\\\\
où A =
$\begin{pmatrix}
a_{11} & \cdots & a_{1n}\\
\vdots &  & \vdots\\
a_{m1} & \cdots & a_{mn}
\end{pmatrix}$
 est la \hyperref[sec:coefficients-sys]{matrice des coefficients du système},\\\\\\
X =
$\begin{pmatrix}
x_{1}\\
\vdots\\
x_{n}
\end{pmatrix}$
 est la matrice-colonne des inconnues, et\\\\\\
b =
$\begin{pmatrix}
b_{1}\\
\vdots\\
b_{m}
\end{pmatrix}$
 est la matrice-colonne des \hyperref[sec:matrice-complete]{termes indépendants}.

\subsubsection{En Pratique}
Imaginons une multiplication de plusieurs matrices notée:
\begin{center}
$Y = A \cdot B \cdot C$
\end{center}
où
\begin{center}
$A =
\begin{pmatrix}
1  & 0  & -1 & 0  \\
0  & 1  & 0  & -1
\end{pmatrix}$;\quad
$B =
\begin{pmatrix}
144  & 54 & 125 & 30 \\
54   & 81 & 24  & 50 \\
125  & 24 & 169 & 42 \\
30   & 50 & 42  & 64
\end{pmatrix}$ \quad
et \quad $C =
\begin{pmatrix}
1  & 0 \\
0  & 1 \\
-1 & 0 \\
0  & -1
\end{pmatrix}$.
\end{center}
Nous avons que
\begin{center}
$\begin{array}{rcl}
Y &=&
\begin{pmatrix}
19 & 30 & -44 & -12 \\
24 & 31 & -18 & -14
\end{pmatrix} \cdot C\\
Y &=&
\begin{pmatrix}
63  & 42\\
42  & 45
\end{pmatrix}
\end{array}$
\end{center}
\textbf{NB:} Notons que dans cet exemple:
\begin{itemize}
\item \hyperref[sec:transpose]{$A^{T} = C$}
\item $B$ est \hyperref[sec:type-matrice]{symétrique}
\end{itemize}

\subsection{Transposition}
\label{sec:transpose}
La matrice $A^{t}$ (ou $A^{T}$, ou $A'$) est appelée transposée de la matrice A; elle est obtenue en écrivant en colonnes les lignes de A et vice-versa [ou en effectuant une symétrie orthogonale sur la diagonale principale de la matrice] :\\
Si A =
$\begin{pmatrix}
1  & 2  & 3  & 4  \\
5  & 6  & 7  & 8  \\
9  & 10 & 11 & 12 \\
13 & 14 & 15 & 16
\end{pmatrix}$
;\\\\\\
Alors, la matrice transposée $A^{t}$ =
$\begin{pmatrix}
1 & 5 &  9 & 13\\
2 & 6 & 10 & 14\\
3 & 7 & 11 & 15\\
4 & 8 & 12 & 16
\end{pmatrix}$\\
\textbf{NB:} Notons aussi que $(A^T)^T = A$

\subsection{Inversion}
\label{sec:inverse}
Une matrice A $\in$ $\mathbb{K}^{m{\times}n}$ est dite inversible à gauche (resp. à droite) s'il existe une matrice B $\in$ $\mathbb{K}^{n{\times}m}$ telle que $B \cdot A = I_{n}$ (resp. $A \cdot B = I_{m}$).\\
Toute matrice B satisfaisant ces deux conditions (inversible à gauche ET à droite) est appelée: inverse de A.\\
De plus, toute matrice-ligne (resp. matrice-colonne) non nulle est inversible à droite (resp. à gauche).
\begin{center}
$\begin{pmatrix}
a_1 & \cdots & a_n
\end{pmatrix}$
 .
$\begin{pmatrix}
b_1\\
\vdots\\
b_n
\end{pmatrix}$
 = $I_1$
\end{center}
La matrice inverse de A se note $\Rightarrow A^{-1}$\\\\
\textbf{NB:}
\begin{itemize}
\item \textbf{Une matrice est inversible ssi son \hyperref[sec:det-base]{déterminant} est $\neq$ de $0$.}
\item \textbf{Pour qu'une matrice soit inversible à gauche et à droite, il faut nécessairement qu'elle soit carrée.}
\end{itemize}

\subsubsection{En Pratique}
\subsubsubsection{\emph{Pour inverser une matrice}}
Une manière d’inverser une matrice $P \in \mathbb{R}^{2 \times 2}$ est de former une matrice $\mathbb{R}^{2 \times 4}$ en combinant la matrice $P$ et la
matrice unité (les dimensions doivent correspondre) comme ceci :
\begin{center}
$\left(
\begin{array}{cc|cc}
1 & 1 & 1 & 0\\
0 & 1 & 0 & 1
\end{array}
\right)$
\end{center}
Ensuite, on applique aux deux parties les mêmes opérations élémentaires afin de ramener la partie gauche à la forme réduite de Gauss-Jordan. Si la partie à gauche correspond à la matrice unitaire, alors la matrice est inversible, sinon elle ne l’est pas.\\
\textbf{OU}\\
On peut aussi trouver l'inverse de toute \hyperref[sec:regulier]{matrice carrée régulière} (voir sous-section~\ref{sec:regulier}) grâce à la formule:
\begin{center}
$A^{-1}\ =\ (\hyperref[sec:det-base]{\det (A)})^{-1}\ .\ (\hyperref[sec:cof]{cof(A)})^t$  (voir section~\ref{sec:cof})
\end{center}


\subsection{Matrices élémentaires}
\label{sec:matrice-elem}
Une matrice élémentaire de type I est une matrice qui ne diffère d'une matrice unité que par une seule entrée située hors de la diagonale principale. Une telle matrice est donc carré, et de la forme:
\begin{center}
E =
$\begin{pmatrix}
1 & & & & \\
 & 1 & & & \\
 & & \ddots & & \\
 & & & \lambda & \\
 & & & & 1
\end{pmatrix}$\\
\end{center}
où l'entrée $\lambda$ est non nulle et d'indice i,j avec i $\neq$ j, les autres entrées hors de la diagonale principale étant nulles.

\subsection{Matrices carrées régulières}
\label{sec:regulier}
Une \hyperref[sec:type-matrice]{matrice carrée} est régulière ssi elle est \hyperref[sec:inverse]{inversible}, càd
\begin{center}
$\det A \neq 0$.
\end{center}
Si une matrice carrée satisfait les conditions équivalentes:
\begin{enumerate}
\item[\emph{a)}] A est inversible (à gauche et à droite);
\item[\emph{b)}] A est un produit de matrices élémentaires;
\item[\emph{c)}] Toute matrice U sous forme réduite de Gauss-Jordan obtenue en effectuant des opérations élémentaires sur les lignes de A est la matrice unité $I_{n}$;
\item[\emph{d)}] La matrice $I_{n}$ peut être obtenue en effectuant des opérations élémentaires sur les lignes de A;
\item[\emph{e)}] Le système homogène A . X = 0 n'admet que \hyperref[sec:trivial]{la solution triviale};
\item[\emph{f)}] Pour tout b $\in$ $\mathbb{K}^{n}$, le système A. X = b admet une et une seule solution.
\end{enumerate}
Alors cette matrice carrée est qualifiée de régulière (ou d'\hyperref[sec:inverse]{inversible}). Les matrices non régulières sont appelées singulières.

\subsection{Autres concepts}
\textit{Cette sous-section rassemble les différents concepts liés de près ou de loin aux matrices (toutes parties confondues) et qui ne sont pas définis dans les autres sections.}\\
\begin{itemize}
\item La \textbf{trace d'une matrice carrée} $A$, noté Tr($A$), est définie comme la somme des coefficients présents sur la diagonale principale de $A$.\\
Ainsi, si $A =
\begin{pmatrix}
3 & 3 & 4\\
1 & 2 & 0\\
0 & 1 & 7
\end{pmatrix}$,
alors
Tr($A$) $=$ $3 + 2 + 7 = 12$
\end{itemize}

\section{Déterminant des matrices carrées}
\label{sec:det-base}
À chaque matrice carrée A sur un corps $\mathbb{K}$, on propose d'associer un élément de $\mathbb{K}$, appelé déterminant de A, noté dét A.
\begin{itemize}
\item Le déterminant d'une matrice permet de déterminer si cette matrice est inversible ou non.
\item Il permet aussi de trouver l’aire (resp. le volume) d'un parallélogramme construit sur deux (resp. trois) vecteurs.
\end{itemize}
\subsection{Règles élémentaires}
\begin{enumerate}
\item Le déterminant de la matrice identitée est égal à 1:
\begin{flushleft}
$\det I_n = 1$
\end{flushleft}

\item Si 2 lignes d'une matrice sont identiques, alors son déterminant vaut 0:
\begin{flushleft}
$\det
\begin{pmatrix}
a_1*\\
a_i*\\
a_j*\\
a_n*
\end{pmatrix}
= 0$,\quad \quad si $a_i* = a_j*$\quad pour $i \neq j$
\end{flushleft}

\item
$\det
\begin{pmatrix}
a_1*\\
\alpha x + \beta y\\
a_n*
\end{pmatrix}
\quad =\quad
\alpha \cdot \det
\begin{pmatrix}
a_1*\\
x\\
a_n*
\end{pmatrix}
+ \beta \cdot \det
\begin{pmatrix}
a_1*\\
y\\
a_n*
\end{pmatrix}$

\item $\det
\begin{pmatrix}
a & b\\
c & d
\end{pmatrix}^t \quad =\quad
\det
\begin{pmatrix}
a & b\\
c & d
\end{pmatrix}$

\item $\det
\begin{pmatrix}
c & d\\
a & b
\end{pmatrix}\quad =\quad cb-ad\quad = \quad
-\det
\begin{pmatrix}
a & b\\
c & d
\end{pmatrix}$
\end{enumerate}

\subsection{En Pratique}
\subsubsection{\emph{Pour trouver le déterminant des matrices carrées d'ordres 1 et 2}}
Pour les matrices d'ordres 1 ou 2, cela est facile. On pose:\\
\begin{center}
det (a) = a \quad \quad pour a $\in \mathbb{K}$\\
\end{center}
et
\begin{center}
det
$\begin{pmatrix}
a & b\\
c & d
\end{pmatrix}$
 = ad -- bc \quad \quad pour a, b, c, d $\in \mathbb{K}$;
\end{center}

\subsubsection{\emph{Pour trouver le déterminant des matrices carrées d'ordre n}}
Pour des matrices carrées d'ordre n, la méthode des diagonales pour calculer le déterminant devient rapidement très compliquées lorsque n augmente.\\
Ainsi, il existe d'autres méthodes qui ne prennent pas en compte la taille de n:\\

\subsubsubsection{Méthode des cofacteurs}
Appelée aussi la méthode des mineures ou la méthode de Laplace\\
Exemple: Si on a une matrice G =
$\begin{pmatrix}
1 & 0 & 1\\
2 & 1 & 1\\
0 & 3 & 1
\end{pmatrix}$
 , alors:\\\\
G = 1.
$\begin{pmatrix}
1 & 1\\
3 & 1
\end{pmatrix}$
 -- 0.
$\begin{pmatrix}
2 & 1\\
0 & 1
\end{pmatrix}$
 + 1.
$\begin{pmatrix}
2 & 1\\
0 & 3
\end{pmatrix}$\\\\
En retrouvant deux matrices d'ordre 2, nous pouvons donc appliquer la \hyperref[sec:det-base]{méthode de base} pour trouver leur déterminant respectif (une matrice multipliée par un \hyperref[sec:ev]{scalaire} nul devient une matrice nulle).\\\\
Ce qui nous donne au final \quad $\Rightarrow$ \quad $\det $(G) = -2 - 0 + 6 = 4\\

\subsubsubsection{Règle de Sarrus}
\textbf{\underline{!!! UNIQUEMENT pour les matrice 3x3 !!!}}\\
Exemple: Si on a une matrice G =
$\begin{pmatrix}
1 & 0 & 1\\
2 & 1 & 1\\
0 & 3 & 1
\end{pmatrix}$
 , alors:
\begin{align*}
\det (G) &= [(1.1.1) + (0.1.0) + (1.2.3)] - [(1.1.0) + (0.2.1) + (1.1.3)]\\
&= [1 + 0 + 6] - [0 + 0 + 3]\\
&= 7 - 3\\
&= 4
\end{align*}
La marche à suivre demande de la concentration:
\begin{itemize}
\item Dans le sens de la diagonale principale, il faut additionner chaque diagonale possible DANS CE SENS.
\item Dans le sens OPPOSÉ à la diagonale principale, il faut soustraire chaque diagonale possible DANS CE SENS.
\item Enfin, tous les éléments de chaque diagonale doivent être ensuite multipliés entre eux.
\item Comme la matrice est d'ordre 3 dans ce cas, il doit y avoir 3 éléments dans chaque diagonale prise. Si la diagonale n'est pas complète, il faut reprendre le terme opposé qui va compléter la diagonale.
\end{itemize}

\subsection{Proposition sur le déterminant d'une matrice}
Pour $A,B\ \in\ \mathbb{K}^{n\times n}$,
\begin{enumerate}
\item $\det A \neq 0$ si et seulement si A est \hyperref[sec:regulier]{régulière}
\item $\det (AB) = \det (A) . \det (B)$
\item $\det (A^{t}) = \det (A)$
\end{enumerate}

\subsubsection{Proposition sur le déterminant d'une matrice \hyperref[sec:regulier]{régulière}}
Pour $A,B\ \in\ \mathbb{K}^{n\times n}$ avec A régulière,
\begin{enumerate}
\item $\det (A^{-1})$ = $(\det (A))^{-1}$
\item $\det (ABA^{-1})$ = $\det (B)$
\end{enumerate}

\subsection{Existence du déterminant}
\label{sec:ex-det}
Pour une matrice d'ordre 1, le déterminant est défini par:
\begin{center}
$\det (a) = a$
\end{center}
Pour les matrices d'ordres 2, 3, 4,... ; la définition est légèrement plus complexe:

\begin{enumerate}
\item Fixons un entier n $\geq$ 2.
\item Supposons avoir défini le déterminant des matrices d'ordre $n-1$.
\end{enumerate}
---------------------------------------------------------------
\begin{itemize}
\item Soit A $\in\ \mathbb{K}^{n\times n}$ une matrice carrée d'ordre n.
\item Pour $i,j\ =\ 1,\cdots ,n$, on note $A_{ij}$ la matrice d'ordre $n-1$ qui a été obtenue en supprimant la i-ème ligne et la j-ème colonne de A.\\
\end{itemize}
Pour $k\ =\ 1,\cdots ,n$, on pose:
\begin{center}
$\delta_{k}(A)\ =\ \sum_{l=1}^n (-1)^{l+k} a_{lk}\ .\ \det (A_{lk})$.
\end{center}

\subsubsection{En Pratique}
\subsubsubsection{\emph{Pour trouver un lien entre le déterminant d'une matrice et de sa sous-matrice}}
Dans le cas général, pour $n$ et $c_1,\cdots ,c_n$ quelconques:
\begin{itemize}
\item Établir un lien entre $\det A_{n-1}$ et $\det A_n\quad \Leftrightarrow$
\begin{center}
$A_n =
\begin{pmatrix}
c_n & c_{n-1} & c_{n-2} & \cdots & c_1\\
c_{n-1} & c_{n-1} & c_{n-2} & \cdots & c_1\\
c_{n-2} & c_{n-2} & c_{n-2} & \cdots & c_1\\
\vdots & \vdots & \vdots & \ddots & \vdots\\
c_1 & c_1 & c_1 & \cdots & c_1
\end{pmatrix}$
\end{center}
On observe que
\begin{center}
$A_{n-1} =
\begin{pmatrix}
c_{n-1} & c_{n-2} & \cdots & c_1\\
c_{n-2} & c_{n-2} & \cdots & c_1\\
\vdots & \vdots & \ddots & \vdots\\
c_1 & c_1 & \cdots & c_1
\end{pmatrix}$
\end{center}
Alors, l'on prend la colonne 1 ($c_1$) et on la soustrait par la colonne 2 ($c_2$):
\begin{center}
$A_n =
\begin{pmatrix}
c_n - c_{n-1} & c_{n-1} & c_{n-2} & \cdots & c_1\\
0 & c_{n-1} & c_{n-2} & \cdots & c_1\\
0 & c_{n-2} & c_{n-2} & \cdots & c_1\\
\vdots & \vdots & \vdots & \ddots & \vdots\\
0 & c_1 & c_1 & \cdots & c_1
\end{pmatrix}$
\end{center}
$\Leftrightarrow$ On peut donc déterminer que, sur la colonne 1,
\begin{center}
$\det A_n = (c_n - c_{n-1}) \cdot \det A_{n-1}$.
\end{center}
\end{itemize}

\section{Matrice des cofacteurs}
\label{sec:cof}
Dans la sous-section \ref{sec:ex-det}, nous avons déterminé la valeur du déterminant.\\
Désormais, nous pouvons déterminer que l'expression:\\
$(-1)^{i+j}\ .\ \det (A_{ij})$\\
qui apparaît dans le \hyperref[sec:ex-det]{développements du déterminant} est appelée: cofacteur d'indices i,j de la matrice A.\\\\
La matrice
\begin{center}
cof(A) = $((-1)^{i+j}\ .\ \det (A_{ij})_{1\geq i,j\geq n}$\\
\end{center}
est appelée \textbf{matrice des cofacteurs} (ou \textbf{comatrice}) de A.
\paragraph*{Exemple:}
\begin{center}
$\begin{pmatrix}
a & b\\
c & d
\end{pmatrix}^{-1}
= \dfrac{1}{ad-bc} \cdot
\begin{pmatrix}
d & -b\\
-c & a
\end{pmatrix}$;
\end{center}
\begin{itemize}
\item[•] $ad-bc$ est le \textit{déterminant},
\item[•] $\begin{pmatrix}
d & -b\\
-c & a
\end{pmatrix}$ est la \textit{\textbf{comatrice transposée}}.
\end{itemize}

\subsection{En Pratique}
\subsubsection{\emph{Pour calculer les comatrices des matrices 1$\times$1}}
La comatrice de toute matrice de taille (1,1) est la matrice identité $I_1 = (1)$.
\subsubsection{\emph{Pour calculer les comatrices des matrices 2$\times$2}}
\begin{center}
cof
$\begin{pmatrix}
a & b\\
c & d
\end{pmatrix}$
 =
$\begin{pmatrix}
d & -c\\
-b & a
\end{pmatrix}$
.
\end{center}
\subsubsection{\emph{Pour calculer les comatrices des matrices 3$\times$3}}
\begin{center}
com
$\begin{pmatrix}
a_{11} & a_{12} & a_{13}\\
a_{21} & a_{22} & a_{23}\\
a_{31} & a_{32} & a_{33}
\end{pmatrix}$
 =
$\begin{pmatrix}
+
\begin{vmatrix}
a_{22} & a_{23}\\
a_{32} & a_{33}
\end{vmatrix}
 &
-
\begin{vmatrix}
a_{21} & a_{23}\\
a_{31} & a_{33}
\end{vmatrix}
 &
+
\begin{vmatrix}
a_{21} & a_{22}\\
a_{31} & a_{32}
\end{vmatrix}\\\\
-
\begin{vmatrix}
a_{12} & a_{13}\\
a_{32} & a_{33}
\end{vmatrix}
 &
+
\begin{vmatrix}
a_{11} & a_{13}\\
a_{31} & a_{33}
\end{vmatrix}
 &
-
\begin{vmatrix}
a_{11} & a_{12}\\
a_{31} & a_{32}
\end{vmatrix}\\\\
+
\begin{vmatrix}
a_{12} & a_{13}\\
a_{22} & a_{23}
\end{vmatrix}
 &
-
\begin{vmatrix}
a_{11} & a_{13}\\
a_{21} & a_{23}
\end{vmatrix}
 &
+
\begin{vmatrix}
a_{11} & a_{12}\\
a_{21} & a_{22}
\end{vmatrix}
\end{pmatrix}$
\end{center}
Enfin, il suffit de :
\begin{enumerate}
\item[\textit{(1)}] \hyperref[sec:det-base]{Trouver le déterminant de chaques sous-matrices}
\item[\textit{(2)}] Appliquer les signes aux valeurs trouvées en \textit{(1)}
\end{enumerate}
Pour trouver la comatrice.

\section{Rang des matrices}
\label{sec:rang-matrice}
\paragraph{Définition}
\emph{Le \textbf{rang} d'une matrice \underline{échelonnée}, \hyperref[sec:gauss-jordan]{réduite} ou non, correspond au nombre de lignes possédant un pivot ($\neq 0$).\\
C'est également le rang de la matrice initiale (non échelonnée) car les opérations élémentaires sur les matrices conservent chacune le rang.}

\subsection{En Pratique}
\textit{Cette sous-section fait appel à des notions abordées dans \textbf{la deuxième partie sur les \hyperref[sec:ev]{espaces vectoriels}} !!!}\\\\

\subsubsection{\emph{Pour déterminer le rang d'une matrice}}
On peut déterminer le rang en procédant à une élimination via la \hyperref[sec:gauss-jordan]{méthode de Gauss-Jordan} et en examinant la forme échelonnée obtenue:
\subparagraph{Exemple:}
Soit la matrice:
\begin{center}
A =
$\begin{pmatrix}
1 & 0 & 2 & 3\\
2 & 0 & 4 & 6\\
0 & 2 & 2 & 0\\
1 & 2 & 4 & 3
\end{pmatrix}$
,
\end{center}
Il existe plusieurs manière pour déterminer le rang:

\textbf{(1)} En procédant à une élimination via la méthode de Gauss-Jordan et en examinant la forme échelonnée obtenue. (+ sûr mais + long)

\textbf{(2)} En vérifiant s'il existe des \hyperref[sec:combili]{combinaisons linéaires}. (- sûr mais - long) (voir~\ref{sec:rang-matrice-ev} \nameref{sec:rang-matrice-ev} pour des explications théoriques)\\\\
\textbf{(1)}: En appliquant des opération élémentaires sur la matrice, l'on obtient la matrice échelonnée:
\begin{center}
A =
$\begin{pmatrix}
1 & 0 & 2 & 3\\
0 & 1 & 1 & 0\\
0 & 0 & 0 & 0\\
0 & 0 & 0 & 0
\end{pmatrix}$
\end{center}
Ainsi, le rang correspond au nombre de ses lignes qui sont non nulles, c'est-à-dire de \textbf{\emph{rang 2}}.\\\\
\textbf{(2)}: On appelle $l_{1},l_{2},l_{3},l_{4}$; les vecteurs formés par les quatre lignes de A.\\
On remarque que:

$l_2$ = 2$l_1$. Donc le rang de A est égal à celui de la famille $(l_{1},l_{3},l_{4})$.

$l_{4}=l_{1}+l_{3}$. Donc le rang de $(l_{1},l_{3},l_{4})$ est égal à celui de $(l_{1},l_{3})$.

$l_{1}$ et $l_{3}$ sont linéairement indépendantes.\\ Donc $(l_{1},l_{3})$ est de \textbf{\emph{rang 2}}.

\section{Règle de Cramer}
Le système de n équations à n inconnues, de forme générale :
\begin{center}
$\left\{
\begin{array}{rl}
a_{11} x_1 \ +\ a_{12} x_2 \ + \cdots \ +\ a_{1n} x_n \quad & = \quad \lambda_1 \\
a_{21} x_1 \ +\ a_{22} x_2 \ + \cdots \ +\ a_{2n} x_n \quad & = \quad \lambda_2 \\
&\ \vdots \\
a_{n1} x_1 \ +\ a_{n2} x_2 \ + \cdots \ +\ a_{nn} x_n \quad & = \quad \lambda_n \\
\end{array}
\right.$
\end{center}
est représenté sous la forme d'un produit matriciel :
\begin{center}
$\begin{pmatrix}
a_{11} & a_{12} & \cdots & a_{1n}\\
a_{21} & a_{22} & \cdots & a_{2n}\\
\vdots & \vdots & \ddots & \vdots \\
a_{n1} & a_{n2} & \cdots & a_{nn}
\end{pmatrix}$
$\times$
$\begin{pmatrix}
x_1\\
x_2\\
\vdots \\
x_n
\end{pmatrix}$
=
$\begin{pmatrix}
\lambda_1\\
\lambda_2\\
\vdots \\
\lambda_n
\end{pmatrix}$
$\Leftrightarrow
A . X = \Lambda$
\end{center}
où la matrice $A$, carrée, contient les coefficients des inconnues, le vecteur colonne $X$ contient ces inconnues et le vecteur colonne $\Lambda$ contient les membres de droite des équations du système ; les coefficients et les inconnues font partie d'un même corps commutatif.\\
Le théorème affirme alors que \textbf{le système admet une unique solution si et seulement si sa matrice $A$ est inversible} (déterminant non nul), et cette solution est alors donnée par :
\begin{center}
$x_k = \dfrac{\det(A_k)}{\det(A)}$
\end{center}
où $A_k$ est la matrice carrée formée en remplaçant la k-ième colonne de $A$ par le vecteur colonne $\Lambda$.
\begin{center}
$A_k = (a_{k|ij})$ avec $a_{k|ij} =$
$\begin{array}{rl}
a_{ij} \quad &$si $j \neq k\\
\lambda_i \quad &$si $j = k
\end{array}$
\end{center}
\textbf{Un système carré} (i.e. avec autant d'équations que d'inconnues) \textbf{est dit de Cramer si le déterminant de sa matrice est non nul.}\\
Lorsque le système (toujours carré) n'est pas de Cramer (i.e. lorsque le déterminant de A est nul) : \\

-- Si le déterminant d'une des matrices $A_k$ est non nul, alors le système n'a pas de solution ;

-- La réciproque est fausse : il peut arriver que le système n'ait pas de solution bien que les déterminants $\det(A_k)$ soient tous nuls. Un exemple en est donné par :
\begin{center}
$\begin{array}{rl}
x + y + z &= 1\\
x + y + z &= 2\\
x + y + z &= 3
\end{array}$
\end{center}

\subsection{En Pratique}
\subsubsection{\emph{Pour trouver les inconnues des systèmes d'ordre 2 avec Cramer}}
Si $ad - bc \neq 0$, le système
\begin{center}
$\begin{array}{rl}
ax + by = e\\
cx + dx = f
\end{array}$
\end{center}
a pour unique solution:
\begin{center}
$x = \dfrac{
\begin{array}{|cc|}
e & b\\
f & d
\end{array}}
{\begin{array}{|cc|}
a & b\\
c & d
\end{array}}
= \dfrac{ed - bf}{ad - bc},\quad
y = \dfrac{
\begin{array}{|cc|}
a & e\\
c & f
\end{array}}
{\begin{array}{|cc|}
a & b\\
c & d
\end{array}}
= \dfrac{af - ec}{ad - bc}$.
\end{center}
Exemple numérique:
\begin{center}
$\begin{array}{rl}
4x + 2y &= 24\\\\
2x + 3y &= 16
\end{array}
\Leftrightarrow
\begin{array}{rl}
x = \dfrac{24.3 - 16.2}{8} = \dfrac{40}{8} = 5\\\\
y = \dfrac{4.16 - 2.24}{8} = \dfrac{16}{8} = 2
\end{array}$
\end{center}

\subsubsection{\emph{Pour trouver les inconnues des systèmes d'ordre 3 avec Cramer}}
\begin{center}
$\begin{array}{rl}
a_1x_1 + b_1x_2 + c_1x_3 = d_1\\
a_2x_1 + b_2x_2 + c_2x_3 = d_2\\
a_3x_1 + b_3x_2 + c_3x_3 = d_3
\end{array}$
\end{center}
Posons:
\begin{center}
$A =
\begin{pmatrix}
a_1 & b_1 & c_1\\
a_2 & b_2 & c_2\\
a_3 & b_3 & c_3
\end{pmatrix}
,\quad X =
\begin{pmatrix}
x_1\\
x_2\\
x_3\\
\end{pmatrix}
et \quad \Lambda =
\begin{pmatrix}
d_1\\
d_2\\
d_3
\end{pmatrix}
.$
\end{center}
Le système admet une solution unique si et seulement si $\det(A) \neq 0$:
\begin{center}
$x_1 = \dfrac{\det(A_1)}{\det(A)} = \dfrac{
\begin{array}{|ccc|}
d_1 & b_1 & c_1\\
d_2 & b_2 & c_2\\
d_3 & b_3 & c_3
\end{array}}
{\det(A)}$
\end{center}
\begin{center}
$x_2 = \dfrac{\det(A_2)}{\det(A)} = \dfrac{
\begin{array}{|ccc|}
a_1 & d_1 & c_1\\
a_2 & d_2 & c_2\\
a_3 & d_3 & c_3
\end{array}}
{\det(A)}$
\end{center}
\begin{center}
$x_3 = \dfrac{\det(A_3)}{\det(A)} = \dfrac{
\begin{array}{|ccc|}
a_1 & b_1 & d_1\\
a_2 & b_2 & d_2\\
a_3 & b_3 & d_3
\end{array}}
{\det(A)}$
\end{center}
Ou plus simplement :
\begin{center}
$X =
\begin{pmatrix}
x_1\\
x_2\\
x_3
\end{pmatrix}
 = \dfrac{1}{\det(A)} .
\begin{pmatrix}
\det(A_1)\\
\det(A_2)\\
\det(A_3)
\end{pmatrix}$.
\end{center}
-- Pour que le système n'admette aucune solution, il suffit que :
\begin{center}
$\det(A) = 0 \quad et \quad (\det(A_1) \neq 0 \quad ou \quad \det(A_2) \neq 0 \quad ou \quad \det(A_3) \neq 0)$.
\end{center}
 -- Dans le cas
\begin{center}
$\det(A) = \det(A_1) = \det(A_2) = \det(A_3) = 0$,
\end{center}
on peut avoir, \emph{soit une infinité de solutions}, \emph{soit aucune}.

\newpage
