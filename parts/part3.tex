%----------------------------------------------------------------------------------------
%	PART 3
%----------------------------------------------------------------------------------------
\lhead{APPLICATIONS LINÉAIRES}
\part{Applications linéaires}
\section{Notions fondamentales}
\paragraph*{Définition}
Soient $E$ et $F$, deux \hyperref[sec:ev]{ev} sur un même corps $\mathbb{K}$. Un application $A : E \rightarrow F$ est dite \textbf{linéaire} si les conditions suivantes sont satisfaites:

1. $A(x + y) = A(x) + A(y)$ pour $x,y \in E$.

2. $A(x\alpha) = A(x)\alpha$ pour $x \in E$ et $\alpha \in \mathbb{K}$.\\\\
De manière équivalente, l'application $A$ est linéaire si
\begin{center}
$A(x\alpha + y\beta) = A(x)\alpha + A(y)\beta$
\end{center}
pour $x,y \in E$ et $\alpha,\beta \in \mathbb{K}$.\\\\
De plus, une vérification directe montre que:

-- L'ensemble des applications linéaires de $E$ dans $F$ est un sev de l'espace de toutes les applications de $E$ dans $F$.

-- La composée de deux applications linéaires est linéaire.\\\\
\textbf{-----------}\\\\
La notion suivante est étroitement liée à celle d'application linéaire:\\
Une équation linéaire est une équation de la forme
\begin{center}
$A(X) = b$
\end{center}
où $A : E \rightarrow F$ est une application linéaire entre deux ev $E$ et $F$, et $b \in F$.\\
$\Rightarrow$ Résoudre cette équation consiste à trouver tous les vecteurs $x \in E$ qui sont envoyés sur $b$ par $A$.\\\\
Une telle équation satisfait le \emph{principe de superposition des solutions}:
\paragraph*{Proposition}
Si $x_1,x_2 \in E$ sont respectivement les solutions de $A(X) = b_1$ et $A(X) = b_2$; alors pour $\alpha_1,\alpha_2 \in \mathbb{K}$, le vecteur $\alpha_1x_1 + \alpha_2x_2 \in E$ est solution de $A(X) = \alpha_1b_1 + \alpha_2b_2$.\\
$\Rightarrow$ Ce principe de superposition des solutions n'est donc qu'une reformulation de l'hypothèse que $A$ est linéaire.

\paragraph*{Proposition}
Une application $L$ est linéaire si:
\begin{enumerate}
\item $x \in E\ ;\ y \in E \quad \quad \Leftrightarrow$ ADDITIVITÉ
\begin{flushright}
$\Rightarrow L(x+y) = L(x) + L(y) \in F$
\end{flushright}
\item $x \in E\ ;\ \alpha \in \mathbb{R} \quad \quad \Leftrightarrow$ MULTIPLICATIVITÉ (par un scalaire)
\begin{flushright}
$\Rightarrow L(\alpha x) = \alpha L(x) \in F$
\end{flushright}
\end{enumerate}

\subsection{En Pratique}
\subsubsection{\emph{Pour vérifier si une application est linéaire}}
Nous avons: \underline{$L:\mathbb{R}^2 \rightarrow \mathbb{R}^3 , (x,y) \mapsto (x+y , x-2y , 0)$}.
\begin{center}
$(x,y) \in \mathbb{R}^2$\\
$(u,v) \in \mathbb{R}^2$
\end{center}
\begin{enumerate}
\item[\textbf{(a)}] $L((x,y) + (u,v))\ ?=\ L(x,y) + L(u,v)$\\\\
$\begin{array}{ccl}
L(x+u , y+v) &=& [(x+u + y+v),(x+u - 2(x+v)), 0]\\
&=& [(x+y) + (u+v) , (x+u - 2y-2v), 0]\\
&=& [(x+y) + (u+v) , (x-2y + u-2v), 0]\\
&=& [(x+y, x-2y, 0) , (u+v , u-2v, 0)]\\
&=& L(x,y,0) + L(u,v,0)$\quad \textbf{\emph{OK}}$
\end{array}$\\

\item[\textbf{(b)}] $L(\alpha x, \alpha y)\ ?=\ \alpha L(x,y)$\\\\
$\begin{array}{ccl}
L(\alpha x, \alpha y) &=& [(\alpha x + \alpha y),(\alpha x - 2\alpha y), \alpha 0]\\
&=& [\alpha (x+y, x-2y, 0)]\\
&=& \alpha L(x,y)$\quad \textbf{\emph{OK}}$
\end{array}$\\
\end{enumerate}
Donc, cette application est bien une application linéaire.

\subsection{Noyau et Image}
À toute application linéaire $A : E \rightarrow F$, on fait correspondre un sev de $E$ appelé \textbf{noyau de $A$}, défini par:
\begin{center}
Ker $A = \lbrace x \in E\ |\ A(x) = 0\rbrace$
\end{center}
et un sev de $F$ appelé \textbf{image de $A$}, défini par:
\begin{center}
Im $A = \lbrace A(x) \in F\ |\ x \in E\rbrace$
\end{center}
ou, de manière équivalente, par:
\begin{center}
Im $A = \lbrace y \in F\ |\ \exists x \in E$ tel que $A(x) = y\rbrace$.
\end{center}
$\Rightarrow$ Pour vérifier que $0 \in$ Ker $A$, càd que $A(0) = 0$ ; il faut remplacer $\alpha$ par $0$ dans la relation $A(x\alpha) = A(x)\alpha$.
\paragraph{Proposition}
\emph{Une application $A : E \rightarrow F$ est :\\
\underline{Injective} ssi Ker $A = \lbrace 0\rbrace$;\\
\underline{Surjective} ssi Im $A = F$;\\
\underline{Bijective} ssi injective et surjective.}

\paragraph*{}
La \textit{dimension du noyau} ($\dim$ Ker) équivaut au nombre de paramètre dans la solution du système d'équation $A(x) = 0$ (au sens vectoriel [x et 0 sont des vecteurs])
\begin{center}
\underline{\textbf{dim Im A + dim Ker A = dim E}}\\(voir \hyperref[sec:rang-null]{Rang et Nullité}).
\end{center}

\subsubsection{Nullité et Rang}
\label{sec:rang-null}
\begin{itemize}
\item[] La \textbf{nullité} de $A$ est la \hyperref[sec:dimension]{dimension} du noyau de $A$:
\begin{center}
null $A = \dim$ Ker $A$.
\linebreak
\end{center}
\item[] Le \textbf{rang} d'une application linéaire $A : E \rightarrow F$ est la \hyperref[sec:dimension]{dimension} de l'image de $A$:
\begin{center}
rang $A = \dim$ Im $A$.
\linebreak
\end{center}
\item[] Ainsi, l'on peut déduire que
\begin{center}
\underline{\textbf{null A + rang A = dim E}}.
\end{center}
\end{itemize}

\subsubsection{En Pratique}
\begin{center}
$L: \mathbb{R}[x]_{\leqslant 2} \rightarrow \mathbb{R}^2 : P(x) \mapsto L(P(x)) = [P(0), P'(0) + P''(0)]$
\end{center}
\subsubsubsection{\emph{Pour déterminer le noyau d'une application linéaire, sa base et sa dimension}}
\begin{itemize}
\item[•] $\textbf{Ker\ L} = \Bigg\lbrace P(x) = ax^2 + bx + c\ |
\begin{pmatrix}
P(0)\\
P'(0) + P''(0)
\end{pmatrix}
=
\begin{pmatrix}
c\\
b + 2a
\end{pmatrix}
=
\begin{pmatrix}
0\\
0
\end{pmatrix}
\Bigg\rbrace\\
Ux =
\begin{pmatrix}
0 & 0 & 1\\
2 & 1 & 0
\end{pmatrix}
\begin{pmatrix}
a\\
b\\
c
\end{pmatrix}
=
\begin{pmatrix}
0\\
0
\end{pmatrix}
\Leftrightarrow
\begin{array}{rl}
c &=\ 0\\
2a + b &=\ 0
\end{array}
\Leftrightarrow
\begin{array}{rl}
c &=\ 0\\
b &=\ -2a
\end{array}\\
S = \lbrace (\alpha, -2 \alpha, 0)\ |\ \alpha \in \mathbb{R}\rbrace$\\
\item Ainsi, \underline{$\emph{Ker\ L = sev <(1, -2, 0)>}$} où \underline{$\emph{(1, -2, 0)}$ est une \textbf{base} de $\emph{Ker\ L}$}\\
\item Et donc \underline{$\emph{null\ L = 1}$} (car il n'y a que \textbf{un seul paramètre} dans la solution du système [à savoir $\alpha$]).\\
\item \textbf{NB:} Dans certains cas, il est intéressant d'échelonner la matrice $U$ (si elle ne l'est pas déjà) pour obtenir un système plus simple à résoudre.
\end{itemize}

\subsubsubsection{\emph{Pour déterminer l'image d'une application linéaire, sa base et sa dimension}}
\begin{itemize}
\item[•] $\textbf{Im\ L} = \{ y \in F | L(x) = y, \forall x \in E \}\\
!\ P(x) = ax^2 + bx + c\ |\
L(P(x)) =
\begin{pmatrix}
c\\
2a + b
\end{pmatrix}
,\ Im\ L = \Bigg\lbrace
\begin{pmatrix}
\alpha\\
\beta
\end{pmatrix}
|\ \alpha, \beta \in \mathbb{R} \Bigg\rbrace$\\
\item Ainsi, $Im\ L = sev<(1, 0), (0, 1)>$\\
\item Et donc \underline{$\emph{rang\ L = 2}$} (car il y a \textbf{deux vecteurs} dans la solution du système)\\
\item Les \textbf{deux vecteurs} de la base de $Im\ L$ forment en réalité la base canonique de l'espace $F$,\\
c'est-à-dire, l'espace $\mathbb{R}^2$\\
c'est-à-dire, la matrice identité $I_2$
\end{itemize}

\subsection{Autres concepts}
\textit{Cette sous-section rassemble les différents concepts liés de près ou de loin aux applications linéaires (toutes parties confondues) et qui ne sont pas définis dans les autres sections.}\\
\begin{itemize}
\item Un \textbf{endomorphisme linéaire} ou \textbf{endomorphisme d'espace vectoriel}, noté $End(E)$ ou $L(E)$, est une application linéaire d'un espace vectoriel $E$ dans lui-même.
\end{itemize}

\section{Contruction d'applications linéaires}
Soient $E$ et $F$, deux ev sur un même corps $\mathbb{K}$. On suppose que $E$ est de dimension finie.

\subsection{Inversibilité des applications linéaires}
-- L'application linéaire $A : E \rightarrow F$ est \hyperref[sec:inversible]{inversible} à gauche (resp. à droite) s'il existe une application linéaire $B : F \rightarrow E$, appelée inverse de A à gauche (resp. à droite), telle que $B \circ A = I_E$ (resp. $A \circ B = I_F$).\\\\
-- L'application linéaire $A : E \rightarrow F$ est \hyperref[sec:inversible]{inversible} s'il existe une application linéaire $B : F \rightarrow E$, appelée inverse de A, telle que $B \circ A = I_E$ et $A \circ B = I_F$.\\\\
\textit{$A \circ B$ est la composée de fonction de $A$ et de $B$.}\\\\
$\Rightarrow$ Bien entendu, si une application linéaire est inversible, elle est à la fois inversible à gauche et à droite, et inversément. Aussi, elle admet un unique inverse à gauche, qui est aussi l'unique inverse à droite et donc plus simplement l'unique inverse.\\\\
En effet, si l'on a $B \circ A = I_E$ et $A \circ B' = I_F$, alors on peut calculer la composée $B \circ\ A \circ B'$ de deux manière différentes:
\begin{center}
$(B \circ A) \circ B' = I_E \circ B' = B'$\\
ET\\
$(B \circ (A \circ B') = B \circ I_F = B$\\
\end{center}
donc $B = B'$.\\
L'inverse d'une application A (INVERSIBLE!!!) est notée $A^{-1}$.
\paragraph{Proposition}
\emph{Pour qu'une application linéaire soit inversible à gauche (resp. à droite), il faut et il suffit qu'elle soit injective (resp. surjective).
\paragraph*{}
Pour qu'une application linéaire soit inversible, il faut et il suffit qu'elle soit bijective.}

\paragraph{Corollaire}
\emph{Soit $A : E \rightarrow F$ une application linéaire. On suppose $\dim E = n$ et $\dim F = m$ (avec $m,n$ finis). Respectivement, les conditions (1) à (4) et (1') à (4') sont équivalentes:}
\begin{center}
$\begin{array}{lll}
$1) A est injective $& &$ 1') A est surjective$\\
$2) A est inversible à gauche $& &$ 2') A est inversible à droite$\\
$3) null A = 0 $& &$ 3') null A = n - m$\\
$4) rang A = n $& &$ 4') rang A = m$
\end{array}$
\end{center}
\emph{En particulier, si $n = m$, alors toutes les conditions ci-dessus sont équivalentes ; ce qui revient à dire que $A$ est inversible et donc que $A$ est bijective.}

\subsection{Représentation matricielle des applications linéaires}
Soient $E$ et $F$, deux ev de dimension finie sur un même coprs $\mathbb{K}$.\\
Soit $e = (e_1,\cdots ,e_n)$, une base de $E$ et $f = (f_1,\cdots ,f_m)$, une base de $F$.\\
À toute application linéaire $A : E \rightarrow F$, on associe une matrice ${}_f(A)_e \in \mathbb{K}^{m\times n}$ par:
\begin{center}
${}_f(A)_e \in \mathbb{K}^{m\times n} = (a_{ij})_{1\leq i\leq m\ ;\ 1\leq j\leq n} \quad$ si $A(e_j) = \sum_{i=1}^m\ f_ia_{ij}$.
\end{center}
La j-ème colonne de la matrice ${}_f(A)_e$ est donc formée des coordonnées par rapport à la base $f$ de $A(e_j)$, l'image par $A$ du j-ème vecteur de la base $e$.\\\\
\textbf{Proposition 1}\\
1. Pour tout scalaire $\alpha$,
\begin{center}
${}_f(\alpha A)_e = \alpha {}_f(A)_e$.
\end{center}
2. Si $A$ et $B$ sont deux applications linéaires de $E$ vers $F$,
\begin{center}
${}_f(A + B)_e = {}_f(A)_e + {}_f(B)_e$.
\end{center}
3. Soit $g = (g_1,\cdots ,g_l)$ une base d'une troisième ev $G$. Si $A : E \rightarrow F$ et $B : F \rightarrow G$ sont des application linéaires,
\begin{center}
${}_g(B\circ A)_e = {}_g(B)_f\ .\ {}_f(A)_e$.
\end{center}

\paragraph{Proposition 2}
Soient $e$ et $f$, respectivement les bases des ev $E$ et $F$. Pour toute application linéaire $A : E \rightarrow F$,
\begin{center}
rang $A =$ rang ${}_f(A)_e$.
\end{center}

\subsection{Changements de base}
Il est important de remarquer que ce qui précède s'applique également dans le cas où $F = E$, et où $A = I_E$ est l'identité sur $E$.\\
Si les bases $e$ et $f$ sont les mêmes, alors un simple calcul montre que l'on obtient bien la matrice unité:
\begin{center}
${}_e(I_E)_e = I_n$;
\end{center}
cependant, si $f \neq e$, alors la matrice ${}_f(I_E)_e$ n'est pas la matrice unité ; c'est la matrice dont la j-ème colonne est formée des coordonnées du vecteur $e_j (= I_E(e_j))$ par rapport à la base $f$. Elle permet de contrôler les changements de base.

\paragraph{Proposition}
Soient $e$ et $e'$, deux bases d'un ev E de dimension $n$ (finie). La matrice de changement de base ${}_{e}(I_E)_{e'}$ est \hyperref[sec:regulier]{régulière}, et
\begin{center}
${}_e(I_E)_{e'}^{-1} = {}_{e'}(I_E)_e$
\end{center}
De plus, pour toute matrice régulière $P \in \mathbb{K}^{n\times n}$ il existe des base $f$ et $f'$ de $E$ telles que
\begin{center}
$\begin{array}{ccc}
P = {}_e(I_E)_f & $et$ & P = {}_{f'}(I_E)_{e'}.
\end{array}$
\end{center}
Ainsi, l'on peut déduire que
\begin{center}
$\begin{array}{cl}
P &=\ \ {}_e(I_E)_f\\
P^{-1} &=\ {}_{e'}(I_E)_{f'}\\
P &=\ {}_{f'}(I_E)_{e'}
\end{array}$
\end{center}
et que
\begin{center}
$\det{}_{e'}(A)_{e'} = \det {}_e(A)_e$.
\end{center}
Ce dernier montre que le déterminant de la matrice de $A$ par rapport à une base de $E$ ne dépend pas du choix de la base. On peut donc poser:
\begin{center}
$\det (A) = \det {}_e(A)_e$,
\end{center}
où $e$ est une base quelconque de $E$. Comme la composée des applications linéaires correspond au produit des matrices qui les représentent, on a pour $A,B : E \rightarrow E$ :
\begin{center}
$\det (A\circ B) = \det A\ .\ \det B$
\end{center}

\newpage
