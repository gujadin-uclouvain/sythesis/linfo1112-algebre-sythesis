%----------------------------------------------------------------------------------------
%	PART 2
%----------------------------------------------------------------------------------------
\lhead{ESPACES VECTORIELS}
\part{Espaces vectoriels}
\label{sec:ev}
\section{Espaces vectoriels (ev)}
Soit $\mathbb{K}$ un corps (commutatif),\\
Soit $\mathbb{E}$ un ensemble muni de deux lois de composition:\\

L'une interne et notée additivement:
\begin{align*}
+\ :\ &\mathbb{E}\ \times\ \mathbb{E} \longrightarrow\ \mathbb{E}\\
&(x,y)\ \longmapsto\ x\ +\ y
\end{align*}

L'autre externe et noté multiplicativement:
\begin{align*}
\cdot \ :\ &\mathbb{E}\ \times\ \mathbb{K} \longrightarrow\ \mathbb{E}\\
&(x,\alpha)\ \longmapsto\ x\alpha
\end{align*}
On dit que $\mathbb{E}$ est un espace vectoriel sur $\mathbb{K}$ si les conditions suivantes sont satisfaites:
\begin{enumerate}
\item[A)] $\mathbb{E}$ est un groupe commutatif pour +.

\item[B)] Pour $x,y\ \in\ E$ et $\alpha,\beta\ \in\ \mathbb{K}$,
\begin{align*}
x(\alpha\ +\ \beta)\ &=\ x\alpha\ +\ x\beta\\
(x\ +\ y)\alpha\ &=\ x\alpha\ +\ y\alpha\\
x(\alpha\beta)\ &=\ (x\alpha)\beta\\
x.1\ &=\ x
\end{align*}
\end{enumerate}
---------------------------------------------------------------
\begin{itemize}
\item Les éléments de $\mathbb{E}$ sont alors appelés \textit{vecteurs},
\item Les éléments de $\mathbb{K}$ sont appelés \textit{scalaires}
\begin{itemize}
\item[•] Les scalaires sont le plus souvent des nombres réels ou, plus généralement, des nombres complexes. On parle alors d'espace vectoriel réel ou complexe, respectivement.
\end{itemize}
\end{itemize}
NB: Le résultat de la multiplication d'un vecteur $x$ et d'un scalaire $\alpha$ est conventionnellement noté $x\alpha$, même si l'on peut aussi l'écrire $\alpha x$.

\subsection{Vecteur nul}
Noté 0, le vecteur nul est caractérisé par la propriété:
\begin{center}
$x + 0 = x$ pour tout $x \in \mathbb{E}$
\end{center}
Ainsi, on montre que
\begin{center}
$x \cdot 0 = 0$
\end{center}
Le symbole "zéro" à gauche est le scalaire nul ($\in \mathbb{K}$) [$\in \mathbb{R}$ dans ce cours],\\
le symbole "zéro" à droite est le vecteur nul ($\in \mathbb{E}$).

\subsection{Combinaisons linéaires}
\label{sec:combili}
Soit $v_1,\cdots ,v_n$, un nombre fini de vecteurs d'un espace vectoriel E.\\
Une combinaison linéaire (ou combili) de $v_1,\cdots ,v_n$ est un vecteur $x \in E$ qui admet une décomposition comme somme de multiples de $v_1,\cdots ,v_n$ :
\begin{center}
$x = v_1\alpha_1 + \cdots + v_n\alpha_n$
\end{center}
pour certains coefficients $\alpha_1,\cdots ,\alpha \in \mathbb{K}$.\\\\
Exemple:
\begin{align*}
(x_1,\cdots ,x_n) + (y_1,\cdots ,y_n)\ &=\ (x_1 + y_1,\cdots ,x_n + y_n)\\
(x_1,\cdots ,x_n)\alpha\ &=\ (x_1\alpha ,\cdots ,x_n\alpha)
\end{align*}
*Valable aussi pour les fonctions, les polynômes, les matrices et les droites sur un plan.

\subsubsection{En Pratique}
\subsubsubsection{\emph{Pour trouver les scalaires d'une expression de plusieurs vecteurs}}
Pour trouver les valeurs de $\alpha_1,\alpha_2,\alpha_3 \in \mathbb{R}$ dans une expression du type
\begin{center}
$w = \alpha_1 \cdot v_1 + \alpha_2 \cdot v_2 + \alpha_3 \cdot v_3$,
\end{center}
et dans le cas où :\\
\begin{center}
$v_1$ =
$\begin{pmatrix}
1\\
1\\
1
\end{pmatrix}$
, $v_2$ =
$\begin{pmatrix}
-1\\
1\\
1
\end{pmatrix}$
, $v_3$ =
$\begin{pmatrix}
1\\
0\\
-1
\end{pmatrix}$
, $w$ =
$\begin{pmatrix}
1\\
0\\
0
\end{pmatrix}$
;
\end{center}
Il suffit de:
\begin{enumerate}
\item[\emph{(1)}] Assembler $v_1$, $v_2$ et $v_3$ en une matrice et d'y ajouter $w$ en tant que colonne des termes indépendants.
\item[\emph{(2)}] Simplifier la matrice principale en une matrice de Gauss-Jordan, grâce aux opérations élémentaires, pour trouver les valeurs de $\alpha_1$, $\alpha_2$ et de $\alpha_3$.
\end{enumerate}
Si nous suivons cette démarche, nous avons alors:
\begin{center}
$\begin{pmatrix}
\begin{array}{lcc|c}
1 & -1 & 1 & 1\\
1 & 1 & 0 & 0\\
1 & 1 & -1 & 0
\end{array}
\end{pmatrix}$
=
$\begin{pmatrix}
\begin{array}{lcc|c}
1 & 0 & 0 & \frac{1}{2}\\
0 & 1 & 0 & \frac{-1}{2}\\
0 & 0 & 1 & 0
\end{array}
\end{pmatrix}$
\end{center}
Ce qui nous donne comme solution:
\begin{center}
S = $\lbrace \alpha_1, \alpha_2, \alpha_3 | \frac{1}{2}, \frac{-1}{2}, 0\rbrace$\\
\end{center}
\textbf{NB:} Lors de \textbf{l'assemblage des vecteurs $v$ en une matrice $A$}, il est préférable de mettre les éléments $x$ des vecteurs $v$ sur \textbf{les colonnes de la matrice}, c'est-à-dire
\begin{center}
$A = (v_1\ v_2\ v_3) = (x_{1*}\ x_{2*}\ x_{3*}) =
\begin{pmatrix}
\begin{array}{lcc}
x_{11} & x_{21} & x_{31}\\
x_{12} & x_{22} & x_{32}\\
x_{13} & x_{23} & x_{33}
\end{array}
\end{pmatrix}$
\end{center}

\section{Sous-espaces vectoriels (sev)}
\label{sec:sev}
\subsection{Notions fondamentales}
Une partie $V$ d'un espace vectoriel E sur un corps $\mathbb{K}$ est un sous-espace vectoriel (ou plus simplement, un sous-espace)(ou en abrégé, sev) si les conditions suivantes sont respectées:

\textbf{1.} $0 \in V$.

\textbf{2.} Pour $x \in V$ et $\alpha \in \mathbb{K}$, on a $x\alpha \in V$.

\textbf{3.} Pour $x,y \in V$, on a $x + y \in V$.\\\\
De manière équivalente, un sous-espace vectoriel de E est une partie non vide de E qui est stable par combinaison linéaires:

Pour $v_1,\cdots ,v_n \in V$,

Pour $\alpha_1,\cdots ,\alpha_n \in \mathbb{K}$,
\begin{center}
$v_1\alpha_1 + \cdots  + v_n\alpha_n \in V$.
\end{center}
De plus, une vérification directe montre que tout sous-espace vectoriel est lui-même un espace vectoriel.\\
Exemple:

-- L'ensemble des solutions d'un système de $m$ équations linéaires homogènes à $n$ inconnues est un sous-espace vectoriel de $\mathbb{K}^n$.\\\\
*Valable aussi pour les fonctions, les polynômes, les droites sur un plan et les matrices.

\subsection{Opérations sur les sous-espaces vectoriels}
\emph{\textbf{A. Intersection:}}

Si $V_1,\cdots ,V_n$ sont les \hyperref[sec:sev]{sev} d'un \hyperref[sec:ev]{ev} E, l'intersection $V_1 \cap \cdots  \cap V_n$ est un \hyperref[sec:sev]{sev} de E.\\\\
\emph{\textbf{B. Somme:}}

Si $V_1,\cdots ,V_n$ sont les sev d'un ev E, on définit la somme de $V_1,\cdots ,V_n$ par:
\begin{center}
$V_1 + \cdots  + V_n = \lbrace v_1 + \cdots  + v_n\ |\ v_1 \in V_1,\cdots ,v_n \in V_n \rbrace$
\end{center}
Une vérification directe montre que cet ensemble est un sev de E.\\

On dit que la somme $V_1,\cdots ,V_n$ est directe si la condition suivante est satisfaite:
\begin{center}
Pour $v_1 \in V_1,\cdots ,v_n \in V_n$,\\
$v_1 + \cdots  + v_n = 0 \quad \Rightarrow \quad v_1 = \cdots  = v_n = 0$
\end{center}
C'est-à-dire qu'il n'y a pas de combinaisons linéaires entre les vecteurs.\\
La somme $V_1 + \cdots  + V_n$ est alors notée $V_1 \oplus \cdots  \oplus V_n$.\\\\
\textbf{EN RÉSUMÉ: Deux sous-espaces $E_n$ et $F_n$ sont en sommes directes si leur intersection est le vecteur nul.}
\begin{center}
$E_n \cap F_n = \{0_n\} \quad \Rightarrow \quad E_n \oplus F_n$
\end{center}

\subsection{Sous-espaces vectoriels engendrés}
\label{sec:engendre}
Le sev $V$ ($\in \mathbb{E}$) est appelé \textit{sous-espace vectoriel engendré par $v_1,\cdots ,v_n$} et est noté $sev\langle v_1,\cdots ,v_n\rangle$.\\\\
Il est le plus petit sev de E contenant $v_1,\cdots ,v_n$, puisque tout sev qui contient $v_1,\cdots ,v_n$ contient aussi les combinaisons linéaires de ces vecteurs.\\
En résumé, un sous-espace vectoriel engendré n'a pas de combinaison linéaire entre ses vecteurs $v_1,\cdots ,v_n$.

\subsection{En Pratique}
\subsubsection{\emph{Pour prouver qu'un ensemble quelconque est un sev}}
Il faut vérifier si l'ensemble quelconque possède les caractéristiques d'un sev (voir plus haut):\\\\
Dans $\mathbb{R}^4$, on considère l'ensemble vectoriel
\begin{center}
$E := \lbrace(x_1,x_2,x_3,x_4)$ t.q $x_1 + x_2 + x_3 + x_4 = 0\rbrace$.
\end{center}
\begin{enumerate}
\item Remplacer les inconnues ($x$) par 0 et vérifier si l'équation est juste:
\begin{flushright}
$(0,0,0,0) \in E$ puisque $0 + 0 + 0 + 0 = 0$\\
\textbf{OK}
\end{flushright}

\item Soient $x \in E$ et $\alpha \in \mathbb{R}$, vérifier si:
\begin{flushright}
$\alpha x_1 + \alpha x_2 + \alpha x_3 + \alpha x_4 = \alpha (x_1 + x_2 + x_3 + x_4)$\\
\textbf{OK}
\end{flushright}

\item Soient $x,y \in E$, vérifier si:
\begin{flushright}
$(x_1 + y_1) + (x_2 + y_2) + (x_3 + y_3) + (x_4 + y_4) = (x_1 + x_2 + x_3 + x_4) + (y_1 + y_2 + y_3 + y_4)$\\
\textbf{OK}\\
\end{flushright}
\end{enumerate}
\underline{\textbf{Ainsi, nous avons prouvé que $E \in \mathbb{R}^4$.}}

\subsubsection{\emph{Pour prouver que deux sev sont égaux}}
Si nous avons

$E$ le sev de $\mathbb{R}^3$ engendré par les vecteurs $v_1 = (2, 3, -1)$ et $v_2 = (1, -1, -2)$, noté $sev\langle v_1, v_2\rangle$,

$F$ le sev de $\mathbb{R}^3$ engendré par les vecteurs $w_1 = (3, 7, 0)$ et $w_2 = (5, 0, -7)$, noté $sev\langle w_1, w_2\rangle$;\\\\
Il suffit de montrer qu'il est possible d'écrire les vecteurs d'un des deux sev comme combinaison linéaire des vecteurs de l'autre sev.\\
L'on remarque dans notre exemple que
\begin{center}
$w_1 = 2v_1 - v_2$\\
$w_2 = v_1 + 3v_2$
\end{center}
Ainsi, tout les éléments de $E$ peuvent s'écrire comme une combinaison linéaire des vecteurs générateurs de $F$ et inversément.\\
Cela prouve que $E = F$.

\section{Bases}
Soit $e= (e_1,\cdots ,e_n)$ une suite de vecteurs d'un ev E sur un corps $\mathbb{K}$.\\

• On dit que la suite $e$ est \textbf{\textit{libre}} (ou que les vecteurs $e_1,\cdots ,e_n$ sont \textit{linéairement indépendants}) ssi il n'y a pas de \hyperref[sec:combili]{combinaisons linéaires} possible entre les différents éléments de la suite $e$.

Pour convention, la suite vide () est aussi considérée comme libre, et on convient que le sev engendré par cette suite est $\lbrace 0 \rbrace$ (qui est en effet le plus petit de tous les sev).\\

• On dit que la suite $e$ est une suite \textbf{\textit{génératice}} de E si le sev engendré par les vecteurs de cette suite génère l'espace $E$ dans son ensemble:
\begin{center}
$sev\langle v_1,\cdots ,v_n\rangle = E$,
\end{center}
ce qui revient à dire que, si \underline{$n$ est le nombre d'éléments dans la suite $e$} et que
\begin{itemize}
\item $n\ <\ $\hyperref[sec:dimension]{dim$(E)$} $\Rightarrow$ $e$ ne sera \textbf{jamais génératrice} de $E$.
\item $n\ =\ $\hyperref[sec:dimension]{dim$(E)$} $\Rightarrow$ si $e$ est \textbf{libre}, alors $e$ sera \textbf{génératrice}.
\item $n\ >\ $\hyperref[sec:dimension]{dim$(E)$} $\Rightarrow$ $e$ n'est \textbf{initialement pas libre}. Après avoir \textbf{rendu la suite libre, si $n\ =\ $dim$(E)$}, alors la suite $e$ sera \textbf{génératrice} mais pas libre (car l'on reprend sa forme initiale).\\
\end{itemize}

• Enfin, on dit que la suite $e$ est une \textbf{\textit{base}} de E si elle est à la fois libre et génératrice.\\
Pour convention, la suite vide () est une base de l'espace nul $\lbrace 0 \rbrace$.\\\\
\textbf{NB:}
Si \underline{$e_1$ est une base de $E_1$}  et \underline{$e_2$ est une base de $E_2$}, où \underline{$E_1, E_2 \in E$}, alors, \textbf{l'extension} (pas l'addition) \textbf{de la base $e_1$ avec $e_2$ forme une base de $E$}.

\subsection{Réflexions utiles}
\begin{itemize}
\item Par vecteurs \textit{linéairement indépendants} pour avoir une suite libre, on entend par là deux choses:
\begin{itemize}
\item[•] qu'un des vecteurs ne doit pas être une combinaison linéaire des autres:
\begin{center}
$v_3 = v_1 - v_2\quad \Rightarrow \quad$ suite pas libre.
\end{center}

\item[•] qu'après échelonnement de la matrice, aucune ligne n'est le vecteur nul (il n'y a donc pas de combinaison linéaire).
\end{itemize}
\end{itemize}

\subsection{En Pratique}
\subsubsection{\emph{Pour déterminer si une suite est génératrice}}
Montrer que tout vecteur $v = (x,y) \in \mathbb{R}^2$ peut s'écrire comme une combinaison linéaire des vecteurs $u_1$ = $(1,1)$ et $u_2$ = $(-1,1)$ :
\begin{center}
$\begin{pmatrix}
\begin{array}{lc|c}
1 & -1 & x\\
1 & 1 & y\\
\end{array}
\end{pmatrix}$
$\Leftrightarrow$
$\begin{pmatrix}
\begin{array}{lc|c}
1 & 0 & \frac{(x+y)}{2}\\
0 & 1 & \frac{(y-x)}{2}\\
\end{array}
\end{pmatrix}$
\end{center}
Grâce à des opérations élémentaires sur les matrices, on obtient:
\begin{enumerate}
\item Une unique solution: $\frac{(x+y)}{2}$ et $\frac{(y-x)}{2}\ \cdots$
\item qui est valide peut importe la valeur du $x$ ou du $y$.
\end{enumerate}

\subsubsection{\emph{Pour déterminer si une suite est libre}}
Montrer que $u_1$ = $(1,1)$ et $u_2$ = $(-1,1)$ sont linéairement indépendants :
\begin{center}
$\begin{pmatrix}
\begin{array}{lc|c}
1 & -1 & 0\\
1 & 1 & 0\\
\end{array}
\end{pmatrix}$
$\Leftrightarrow$
$\begin{pmatrix}
\begin{array}{lc|c}
1 & 0 & 0\\
0 & 1 & 0\\
\end{array}
\end{pmatrix}$
\end{center}
Grâce à des opérations élémentaires sur les matrices, on observe que les vecteurs $u_1$ et $u_2$ sont linéairement indépendants (c'est-à-dire qu'aucun des deux vecteurs n'est la combinaison linéaire de l'autre) (pas de vecteur nul après opérations élémentaires).\\

\underline{Contre-exemple:}\\
Montrer que $u_1$ = $(1,2,3)$, $u_2$ = $(-1,-1,1)$ et $u_3$ = $(1,3,7)$ sont linéairement indépendants :
\begin{center}
$\begin{pmatrix}
\begin{array}{lcc|c}
1 & -1 & 1 & 0\\
2 & -1 & 3 & 0\\
3 & 1 & 7 & 0
\end{array}
\end{pmatrix}$
$\Leftrightarrow$
$\begin{pmatrix}
\begin{array}{lcc|c}
1 & 0 & 2 & 0\\
0 & 1 & 1 & 0\\
0 & 0 & 0 & 0
\end{array}
\end{pmatrix}$
\end{center}
Les trois vecteurs ne sont pas linéairement indépendant car:

Le troisième vecteur est combinaison linéaire des deux premiers,

DONC qu'il est devenu un vecteur nul,

ET DONC qu'il n'y a que deux pivots sur une matrice à trois vecteurs.

\subsubsection{\emph{Distinctions pratiques entre « générateur » et « libre »}}
Prenons un ev $E = \mathbb{R}^3$ et essayons de déterminer:

\textbf{-- Une suite libre qui n'est pas génératrice:}\\
\emph{Si le nombre d'éléments dans la suite $\neq$ dimension de E, exemple:}
\begin{flushright}
La suite $e$ composée d’un unique élément $v_1 = (1,0,0)$ est\\\underline{libre mais pas génératrice de $\mathbb{R}^3$}.
\end{flushright}

\textbf{-- Une suite génératrice qui n'est pas libre:}\\
\emph{Si un seul des vecteurs est la somme (resp. différence) de plusieurs OU si deux vecteurs sont égaux, exemple:}
\begin{flushright}
La suite $e = (v_1,v_2,v_3,v_4)$,\\où $v_1 = (1,0,0)$, $v_2 = (0,1,0)$, $v_3 = (0,0,1)$ et $v_4 = (1,1,0)$,\\est \underline{génératrice
de $\mathbb{R}^3$ mais n’est pas libre} car $v_4 = v_1 + v_2$.
\end{flushright}

\subsubsection{\emph{Pour trouver une base du sev d'un ensemble de solutions d'une équation}}
On considère le système homogène $Ax = 0$ de 5 équations à 4 inconnues où
\begin{center}
$A =
\begin{pmatrix}
2 & 3 & 4 & -5\\
1 & 4 & 3 & -4\\
1 & -1 & 1 & -1\\
-2 & 2 & -2 & 2\\
0 & 5 & 2 & -3
\end{pmatrix}
\in \mathbb{R}^{5\times 4}, \quad x =
\begin{pmatrix}
x_1\\
x_2\\
x_3\\
x_4
\end{pmatrix}
\in \mathbb{R}^4$.
\end{center}
Pour trouver une base du sev des solution de $Ax = 0$, il faut:
\begin{enumerate}
\item Échelonner la matrice $A$:
\begin{center}
$A \Rightarrow
\begin{pmatrix}
1 & -1 & 1 & -1\\
0 & 5 & 2 & -3\\
0 & 0 & 0 & 0\\
0 & 0 & 0 & 0\\
0 & 0 & 0 & 0
\end{pmatrix}$
\end{center}
Nous pouvons déjà affirmer que le rang $A$ = $2$.
\item Déterminer quelles sont les variables libres et leurs attribuer des valeurs réelles:\\
Dans notre cas, les variables libres sont $x_3$ et $x_4$ car ce sont les seules valeurs qui ne sont pas des pivots dans notre matrice échelonnée (contrairement à $x_1$ et $x_2$).\\
Ainsi, nous pouvons attribuer à $x_3$ et à $x_4$ des valeurs réelles pour trouver une base de deux vecteurs (car deux variables libres) précis de l'ensemble des solutions.\\
On choisira donc que
\begin{itemize}
\item Pour le vecteur 1 $\Rightarrow$ $x_3 = 1$ et $x_4 = 0$
\item Pour la vecteur 2 $\Rightarrow$ $x_3 = 0$ et $x_4 = 1$
\end{itemize}
\item Trouver les valeurs des variables non-libres:
\begin{center}
$\left\{
\begin{array}{ccccccccc}
x_1 &-& x_2 &+& x_3 &-& x_4 &=& 0\\
&&5x_2 &+& 2x_3 &-& 3x_4 &=& 0
\end{array}
\right.$
\end{center}
On remplace $x_3$ et $x_4$ par les valeurs choisies au point précédent et on isole les variables non-libres:
\begin{itemize}
\item Vecteur 1:
\begin{center}
$\left\{
\begin{array}{ccccccc}
x_1 &-& x_2 &+& 1 &=& 0\\
&&5x_2 &+& 2 &=& 0
\end{array}
\right.
\Leftrightarrow
\left\{
\begin{array}{ccccccc}
x_1 &=& \frac{-7}{5}\\
x_2 &=& \frac{-2}{5}
\end{array}
\right.$
\end{center}
\item Vecteur 2:
\begin{center}
$\left\{
\begin{array}{ccccccc}
x_1 &-& x_2 &-& 1 &=& 0\\
&&5x_2 &-& 3 &=& 0
\end{array}
\right.
\Leftrightarrow
\left\{
\begin{array}{ccccccc}
x_1 &=& \frac{8}{5}\\
x_2 &=& \frac{3}{5}
\end{array}
\right.$
\end{center}
\end{itemize}
\item Écrire l'ensemble de solution:
\begin{center}
$S = sev <((\frac{-7}{5}, \frac{-2}{5}, 1, 0),(\frac{8}{5}, \frac{3}{5}, 0, 1))>$
\end{center}
\end{enumerate}

\section{Dimension}
\label{sec:dimension}
\subsection{Notions fondamentales}
La dimension d'un ev finiment \hyperref[sec:engendre]{engendré} est le nombre d'élément d'une \underline{base} quelconque.
\paragraph{Théorème}
\emph{Toutes les bases d'un espace vectoriel finiment engendré ont le même nombre d'éléments.}
\paragraph{Corollaire}
Dans un ev E de dimension finie n,
\begin{enumerate}
\item de toute suite génératrice, on peut extraire une base;
\item toute suite génératrice de n éléments est une base;
\item toute suite libre peut être prolongée en base;
\item toute suite libre de n éléments est une base.
\end{enumerate}
\paragraph{Proposition: Formule de Grassmann\\}
Si $V$ et $W$ sont deux sev de dimension finie d'un ev $E$,\\
alors, $V + W$ est de dimension finie; ainsi que
\begin{center}
$\begin{array}{rcl}
\dim (V) + \dim (W) &=& \dim (V + W) + \dim (V \cap W)\\
\dim (V + W) &=& \dim (V) + \dim (W) - \dim (V \cap W).
\end{array}$
\end{center}
\textbf{NB:} si un sev engendré $V \in E$ se note $sev\langle v_1,\cdots ,v_n\rangle$, alors \textbf{$v_1,\cdots ,v_n$ est une base de l'espace E} et \textbf{sa dimension est de taille n} (car un sev engendré est le plus petit sev de E [pas de combinaisons linéaires])

\subsection{En Pratique}
\subsubsection{\emph{Pour calculer les dimensions de deux espaces}}
On considère, dans $\mathbb{R}^4$ les vecteurs :
\begin{center}
$v_1 = (1,2,3,4)$, $v_2 = (1,1,1,3)$, $v_3 = (2,1,1,1)$, $v 4 = (-1,0,-1,2)$, $v_5 = (2,3,0,1)$.
\end{center}

Soit $F$, l'ev \hyperref[sec:engendre]{engendré} par $\lbrace v_1,v_2,v_3\rbrace$,

Soit $G$, l'ev \hyperref[sec:engendre]{engendré} par $\lbrace v_4,v_5\rbrace$,\\

Calculez les dimensions de $F$, $G$, $F \cap G$ et $F + G$:\\
\begin{itemize}
\item[\textbf{1.}] Parce que $v_1,v_2,v_3$ et $v_4,v_5$ sont linéairement indépendant, on peut déjà déterminer que:
\begin{center}
$\begin{array}{l}
\dim F = 3$ car $F = sev\langle  v_1,v_2,v_3\rangle\\
\dim G = 2$ car $G = sev\langle  v_4,v_5\rangle\\
\end{array}$
\linebreak
\end{center}

\item[\textbf{2.}] On additionne $F$ et $G$ pour trouver $F + G$ et on fait apparaître les pivots en échelonnant $F + G$:\\
$\begin{pmatrix}
1 & 1 & 2\\
2 & 1 & 1\\
3 & 1 & 1\\
4 & 3 & 1
\end{pmatrix}$
 +
$\begin{pmatrix}
-1 & 2\\
0 & 3\\
-1 & 0\\
2 & 1
\end{pmatrix}$
 =
$\begin{pmatrix}
1 & 1 & 2 & -1 & 2\\
2 & 1 & 1 & 0 & 3\\
3 & 1 & 1 & -1 & 0\\
4 & 3 & 1 & 2 & 1
\end{pmatrix}$
$\quad \Leftrightarrow \quad$
$\begin{pmatrix}
1 & 0 & 0 & 0 & \frac{5}{2}\\
0 & 1 & 0 & 0 & -9\\
0 & 0 & 1 & 0 & 7\\
0 & 0 & 0 & 1 & \frac{11}{2}
\end{pmatrix}$\\
On obtient donc une matrice d'ordre 4, ainsi $\dim (F + G) = 4$.\\

\textbf{NB:} Comme le but est de trouver le nombre de pivots, il n'y a pas besoin de mettre la matrice $F + G$ sous forme réduite de Gauss-Jordan, la forme échelonnée suffit.\\

\item[\textbf{3.}] Selon la formule de Grassmann ci-dessus, nous pouvons trouver facilement $F \cap G$:\\\\
Si on a $\dim (F \cap G) = \dim (F) + \dim (G) - dim (F + G)$,\\
Alors,
\begin{center}
$\begin{array}{rcl}
\dim (F \cap G) &=& (3 + 2) - 4\\
&=& 1
\end{array}$
\end{center}
\end{itemize}

\section{Rang des matrices dans un espace vectoriel}
\label{sec:rang-matrice-ev}
\textit{Cette section met en lien le concept de \hyperref[sec:rang-matrice]{rang des matrices}, vu dans la première partie, avec celui d'espaces vectoriels.}\\\\
Soit A =
$\begin{pmatrix}
a_{11} & \cdots & a_{1n}\\
\vdots & & \vdots\\
a_{m1} & \cdots & a_{mn}
\end{pmatrix}$
$\in \mathbb{K}^{m\times n}$ une matrice quelconque de genre $(m,n)$ sur un corps $\mathbb{K}$, que l'on décompose par ligne:
\begin{center}
$A =
\begin{pmatrix}
a_{1*}\\
\vdots\\
a_{m*}
\end{pmatrix}$
\end{center}
et par colonnes:
\begin{center}
$A =
\begin{pmatrix}
(a_{*1} & \cdots & a_{*n}
\end{pmatrix}$.
\end{center}
Chacune des lignes $a_{i*}$ est un n-uple: $a_{i*} \in \mathbb{K}^n$,\\
on peut considérer le sev de $\mathbb{K}^n$ \hyperref[sec:engendre]{engendré} par ces lignes:
\begin{center}
$\mathcal{L}(A) = sev\langle a_{*1},\cdots,a_{*n}\rangle \subset \mathbb{K}^n$,
\end{center}
ce sev, que l'on appelle \textit{espace des lignes de A}.\\\\
De même, on peut considérer l'\textit{espace des colonnes de A}, défini par:
\begin{center}
$\mathcal{C}(A) = sev\langle a_{1*},\cdots,a_{n*}\rangle \subset \mathbb{K}^m$.
\end{center}
Le théorème suivant indique d'ailleurs que ces sev ont la même dimension:
\paragraph{Théorème}
\emph{Soit $A$ une matrice de dimension $m \times n$ sur un corps $\mathbb{K}$. La dimension du sev de $\mathbb{K}^m$ engendré par les $n$ colonnes de $A$ est égale à la dimension du sev de $\mathbb{K}^n$ engendré par les $m$ lignes de $A$:}
\begin{center}
$\dim \mathcal{C}(A) = \dim \mathcal{L}(A)$
\end{center}
\paragraph{Définition}
\emph{On appelle \textbf{rang} d'une matrice $A \in \mathbb{K}^{m\times n}$ la dimension du sev $\mathcal{C}(A)$ de $\mathbb{K}^m$ engendré par les $n$ colonnes de $A$ ou, ce qui revient au même d'après le théorème précédent, la dimension du sev $\mathcal{L}(A)$ de $\mathbb{K}^n$ engendré par les lignes de $A$.}\\
Grâce à cette définition, l'on détermine que
\begin{center}
$rang\ A\ \leq \min(m,n)$\\
$rang\ A = rang\ A^t$.
\end{center}
L'on remarque que si $rang\ A = n$ alors les lignes de A forment une suite génératrice de $\mathbb{K}^n$, et \textbf{réciproquement}.

\subsection{En Pratique}
\subsubsection{\emph{Pour trouver la dimension de l'espace annulateur d'une matrice}}
Nous avons $A =
\begin{pmatrix}
1 & 2 & 0 & 3\\
0 & 1 & 0 & 1\\
1 & -1 & 1 & 1
\end{pmatrix}$\\
\begin{itemize}
\item Trouver le rang de la matrice $A$:
\begin{center}
$A =
\begin{pmatrix}
1 & 0 & 0 & 1\\
0 & 1 & 0 & 1\\
0 & 0 & 1 & 1
\end{pmatrix}
\Rightarrow
rang(A) = 3$
\end{center}

\item Trouver la dimension de l'espace annulateur de $A$:\\
Pour trouver cela, il faut \textbf{soustraire le nombre de colonnes d'une matrice $n \times m$ par son rang}. Ainsi, nous avons
\begin{center}
$\begin{array}{ccccl}
m &-& $rang$(A) &=& $dimension espace annulateur de $A\\
4 &-& 3 &=& 1
\end{array}$
\end{center}
Dès lors, la dimension de l'espace annulateur de $A$ vaut 1.
\end{itemize}

\newpage
